/*
 * Copyright (c) 2004-2008 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2006 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2007      Sun Microsystems, Inc.  All rights reserved.
 * Copyright (c) 2007-2020 Cisco Systems, Inc.  All rights reserved
 * Copyright (c) 2015-2020 Intel, Inc.  All rights reserved.
 * Copyright (c) 2021-2022 Nanook Consulting.  All rights reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

/**
 * @file
 *
 * Data server for PRTE
 */
#ifndef PRTE_SETOP_SERVER_H
#define PRTE_SETOP_SERVER_H

#include "prte_config.h"
#include "types.h"

#include "src/rml/rml_types.h"
#include "src/pmix/pmix-internal.h"
#include "src/prted/pmix/pmix_server_internal.h"
#include "src/prted/pmix/pmix_server.h"


typedef struct _info_release_cbdata{
    pmix_info_t *info;
    size_t ninfo;
}info_release_cbdata_t;

void prte_setop_server_init(void);
void prte_setop_server_close(void);
bool prte_setop_server_initialized(void);

void prte_setop_server_progress(prte_setop_t *setops, size_t num_setops);

pmix_status_t prte_setop_alloc_request_nb(  pmix_alloc_directive_t directive,
                                pmix_info_t *info, size_t ninfo,
                                pmix_info_cbfunc_t cbfunc, void *cbdata);

pmix_status_t set_highest_job_rank(pmix_nspace_t nspace, pmix_rank_t highest_rank);

pmix_status_t prte_pset_define( char * pset_name, pmix_proc_t *procs, size_t nprocs, 
                                pmix_alloc_directive_t op, pmix_nspace_t nspace, size_t index);

pmix_alloc_directive_t prte_pset_get_op(prte_pset_t *pset);    

prte_setop_t * prte_setop_create(  pmix_alloc_directive_t op, char *input_names, 
                        char *output_names, 
                        pmix_info_t *col, size_t n_col, 
                        pmix_value_t *set_infos, size_t n_set_infos);
pmix_status_t prte_setops_get_ouput_list(prte_setop_t *setops, size_t num_setops, char **outputlist);

pmix_status_t prte_setop_create_launch_op(prte_job_t * job);

#endif /* PRTE_SETOP_SERVER_H */
