/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2011 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2007-2020 Cisco Systems, Inc.  All rights reserved
 * Copyright (c) 2012-2016 Los Alamos National Security, LLC.
 *                         All rights reserved
 * Copyright (c) 2015-2020 Intel, Inc.  All rights reserved.
 * Copyright (c) 2017-2018 Research Organization for Information Science
 *                         and Technology (RIST).  All rights reserved.
 * Copyright (c) 2021-2022 Nanook Consulting.  All rights reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "prte_config.h"
#include "constants.h"
#include "types.h"

#include <string.h>

#ifdef HAVE_SYS_TIME_H
#    include <sys/time.h>
#endif

#include "src/class/pmix_pointer_array.h"
#include "src/pmix/pmix-internal.h"
#include "src/util/pmix_argv.h"

#include "src/mca/errmgr/errmgr.h"
#include "src/rml/rml.h"
#include "src/runtime/prte_globals.h"
#include "src/runtime/prte_wait.h"
#include "src/util/name_fns.h"

#include "src/mca/state/base/base.h"
#include "src/mca/rmaps/rmaps_types.h"
#include "src/mca/odls/base/base.h"
#include "src/mca/ras/base/base.h"

#include "src/runtime/prte_setop_server.h"

static int alloc_counter = 0;
static int dummy_name_ctr = 0;
static char *prte_pset_base_name = "prrte://base_name/";
static pmix_list_t *highest_rank_ever_list = NULL;
static pmix_list_t *delayed_requests;

static bool initialized = false;
static int prte_setop_server_output = -1;
static int prte_setop_server_verbosity = -1;

void prte_setop_alloc_request_cbfunc(pmix_status_t status, pmix_info_t info[], size_t ninfo, void *cbdata, pmix_release_cbfunc_t release_fn, void *release_cbdata);
static void prte_setop_alloc_request_apply(int sd, short args, void *cbdata);

/************************
 * Server Functions 
*************************/
void prte_setop_server_init(){
    prte_pset_t *pset;

    if (initialized) {
        return;
    }
    initialized = true;

    highest_rank_ever_list = PMIX_NEW(pmix_list_t);
    delayed_requests = PMIX_NEW(pmix_list_t);

    /* register a verbosity */
    prte_setop_server_verbosity = -1;
    (void) pmix_mca_base_var_register("prte", "prte", "setop", "server_verbose",
                                      "Debug verbosity for PRTE setop server",
                                      PMIX_MCA_BASE_VAR_TYPE_INT,
                                      &prte_setop_server_verbosity);
    if (0 <= prte_setop_server_verbosity) {
        prte_setop_server_output = pmix_output_open(NULL);
        pmix_output_set_verbosity(prte_setop_server_output, prte_setop_server_verbosity);
    }

    /* Create the NULL PSet for job launches */
    pset = PMIX_NEW(prte_pset_t);
    pset->name = strdup("");
    strcpy(pset->range_specifier, prte_default_session->alloc_refid);
    PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_RANGE_SESSION);
    PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_NULL);
    if(PRTE_SUCCESS != prte_set_pset_object(pset)){
        PRTE_ERROR_LOG(PRTE_ERR_OUT_OF_RESOURCE);
    }
    PMIx_server_define_process_set(NULL, 0, "");
}

bool prte_setop_server_initialized(){
    return initialized;
}

void prte_setop_server_close(){

    if (!initialized) {
        return;
    }
    initialized = false;

    PMIX_RELEASE(highest_rank_ever_list);
    PMIX_RELEASE(delayed_requests);
}

static void prte_setop_server_delay_request(pmix_server_req_t *req){
    pmix_list_append(delayed_requests, &req->super);
}


/* Send request reponses, start delayed requests or delete finished reqs if possible */
void prte_setop_server_progress(prte_setop_t *setops, size_t num_setops){
    pmix_server_req_t *req, *req_delayed, *req2;
    prte_setop_t *req_setops;
    pmix_status_t rc;
    prte_pmix_server_op_caddy_t *caddy;
    int i, j, n_deps;
    size_t n, k;
    bool found, done, error, pending;
    char *tmp;
    char ** deps, **new_deps;

    for(n = 0; n < num_setops; n++){
    pmix_output_verbose(10, prte_setop_server_output, 
                            "%s setop_server_progress: SetOp %s state %d\n",
                            PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), setops[n].alloc_id,
                            setops[n].state);
        /* Check if we need to send a response for the request */
        for(i = 0; i < prte_pmix_server_globals.local_reqs.size; i++){
            req = pmix_pointer_array_get_item(&prte_pmix_server_globals.local_reqs, i);
            if(NULL == req || 0 == req->sz){
                continue;
            }
            req_setops = (prte_setop_t *) req->data;

            error = false;
            found = false;
            pending = false;
            done = true;
            for(k = 0; k < req->sz; k++){
                if(0 == strcmp(req_setops[k].alloc_id, setops[n].alloc_id)){
                    found = true;
                }
                if( req_setops[k].state != PRTE_SETOP_STATE_PENDING &&
                    req_setops[k].state != PRTE_SETOP_STATE_ERROR && 
                    req_setops[k].state != PRTE_SETOP_STATE_FINALIZED )
                {
                    done = false;
                }
                if(req_setops[k].state == PRTE_SETOP_STATE_ERROR){
                    error = true;
                }
                if(req_setops[k].state == PRTE_SETOP_STATE_PENDING){
                    pending = true;
                }
            }

            pmix_output_verbose(101, prte_setop_server_output, 
                            "%s setop_server_progress: SetOp %s found %d done %d error %d\n",
                            PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), setops[n].alloc_id, found, done, error);
            /* Request does not contain setop or is not yet done */
            if(!found || !done){
                continue;
            }

            /* Req is done. Flag for deletion */
            if(!pending){
                req->flag = true;
            }

            /* Setop request has not yet been answered */
            if(!req->inprogress){
                /* Report an error */
                if(error){
                    if(NULL != req->infocbfunc){
                        req->inprogress = true;
                        /* TODO: store error code in setop */
                        req->infocbfunc(setops[n].error_code, NULL, 0, req->cbdata, NULL, NULL);
                    }
                    continue;
                /* Report results */
                }else{
                    rc = prte_setops_get_ouput_list(req_setops, req->sz, &tmp);
                    if(rc != PMIX_SUCCESS){
                        if(NULL != req->infocbfunc){
                            req->infocbfunc(rc, NULL, 0, req->cbdata, NULL, NULL);
                        }
                        return;
                    }

                    /* Send the response */
                    if(NULL != req->infocbfunc){
                        caddy = PMIX_NEW(prte_pmix_server_op_caddy_t);
                        caddy->ninfo = 2;
                        PMIX_INFO_CREATE(caddy->info, 2);
                        PMIX_INFO_LOAD(&caddy->info[0], PMIX_ALLOC_ID, setops[0].alloc_id, PMIX_STRING);
                        PMIX_INFO_LOAD(&caddy->info[1], PMIX_PSETOP_OUTPUT, tmp, PMIX_STRING);
                        free(tmp);
                    
                        pmix_output_verbose(10, prte_setop_server_output, 
                            "%s setop_server_progress: Sending response alloc_id=%s, output=%s\n",
                            PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), caddy->info[0].value.data.string,
                            caddy->info[1].value.data.string);
                        /* req will be released during setop finalization */
                        req->inprogress = true;
                        req->infocbfunc(PMIX_SUCCESS, caddy->info, caddy->ninfo, req->cbdata, info_cb_release, (void*) caddy);
                    }
                }
            }
        }
    }

    /* Delete all reqs that were flagged for deletion */
    for(i = 0; i < prte_pmix_server_globals.local_reqs.size; i++){
        req = pmix_pointer_array_get_item(&prte_pmix_server_globals.local_reqs, i);
        if(NULL == req || 0 == req->sz || !req->flag){
            continue;
        }

        req_setops = (prte_setop_t *) req->data;

        /* Update delayed requests that depend on this setop */
        PMIX_LIST_FOREACH_SAFE(req_delayed, req2, delayed_requests, pmix_server_req_t){
            deps = PMIX_ARGV_SPLIT_COMPAT(req_delayed->cmdline, ',');
            n_deps = PMIX_ARGV_COUNT_COMPAT(deps);
            new_deps = NULL;
            found = false;
            for(j = 0; j < n_deps; j++){
                if(0 == strcmp(deps[j], req_setops[0].alloc_id)){
                    found = true;
                }else{
                    PMIX_ARGV_APPEND_NOSIZE_COMPAT(&new_deps, deps[j]);
                }
            }
            if(found){
                if(NULL == new_deps){
                    pmix_output_verbose(10, prte_setop_server_output, 
                        "%s setop_server_progress: Starting delayed request \n",
                        PRTE_NAME_PRINT(PRTE_PROC_MY_NAME));
                    pmix_list_remove_item(delayed_requests, &req_delayed->super);
                    prte_event_set(prte_event_base, &req_delayed->ev, -1, PRTE_EV_WRITE, prte_setop_alloc_request_apply, req_delayed);
                    PMIX_POST_OBJECT(req_delayed);
                    prte_event_active(&req_delayed->ev, PRTE_EV_WRITE, 1);
                }else{
                    free(req->cmdline);
                    req->cmdline = PMIX_ARGV_JOIN_COMPAT(new_deps, ',');
                }
            }
            PMIX_ARGV_FREE(new_deps);
        }
        

        pmix_output_verbose(10, prte_setop_server_output, 
            "%s setop_server_progress: Releasing request with alloc_id=%s\n",
            PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), req_setops[0].alloc_id);

        /* Release the request and associated setops */
        pmix_pointer_array_set_item(&prte_pmix_server_globals.local_reqs, req->local_index, NULL);
        for(n = 0; n < req->sz; n++){
            (void*)prte_remove_setop_object(req_setops[n].alloc_id);
            PMIX_DESTRUCT(&req_setops[n]);
        }
        free(req->data);
        PMIX_RELEASE(req);
    }
}

/************************
 * JOB UTILITIES 
*************************/
static int get_available_slots(prte_node_t *node, char ** nodes_add, int num_nodes_add, char ** ppr, bool empty_nodes){
    bool found = false;
    int i, slots_allowed = node->slots;

    /* Check if node is contained in nodes_add if provided */
    if(0 < num_nodes_add){
        for(i = 0; i < num_nodes_add; i++){
            if(0 == strcmp(nodes_add[i], node->name)){
                found = true;
                /* Did they specify procs per node? */
                if(NULL != ppr){
                    slots_allowed = atoi(ppr[i]);
                }
                break;
            }
        }
        if(!found){
            return 0;
        }
    }
    if(empty_nodes){
        if(node->slots_inuse == 0){
            return MIN(node->slots, slots_allowed);
        }
    }else if(node->slots != node->slots_inuse){
        return MIN(node->slots - node->slots_inuse, slots_allowed);
    }
    return 0;
}

/* TODO: This should be a job attribute */
pmix_status_t set_highest_job_rank(pmix_nspace_t nspace, pmix_rank_t highest_rank){
    prte_info_item_t *info = NULL;

    if(NULL == highest_rank_ever_list){
        return PMIX_ERR_INIT;
    }
    PMIX_LIST_FOREACH(info, highest_rank_ever_list, prte_info_item_t){
        if(PMIX_CHECK_KEY(&info->info, nspace)){
            PMIX_VALUE_LOAD(&info->info.value, &highest_rank, PMIX_PROC_RANK);
            return PMIX_SUCCESS;
        }
    }

    info = PMIX_NEW(prte_info_item_t);

    PMIX_INFO_LOAD(&info->info, nspace, &highest_rank, PMIX_PROC_RANK);
    pmix_list_append(highest_rank_ever_list, &info->super);
    return PMIX_SUCCESS;
}

static pmix_status_t get_highest_job_rank(pmix_nspace_t nspace, pmix_rank_t *highest_rank){
    prte_info_item_t *info = NULL;

    if(NULL == highest_rank_ever_list){
        return PMIX_ERR_INIT;
    }
    PMIX_LIST_FOREACH(info, highest_rank_ever_list, prte_info_item_t){
        if(PMIX_CHECK_KEY(&info->info, nspace)){
            *highest_rank = info->info.value.data.rank;
            return PMIX_SUCCESS;
        }
    }
    return PMIX_ERR_NOT_FOUND;
}


/****************************
 * PSet Functions 
 ****************************/
pmix_alloc_directive_t prte_pset_get_op(prte_pset_t *pset){
    if(PRTE_FLAG_TEST(pset, PRTE_PSET_FLAG_ADD)){
        return PMIX_PSETOP_ADD;
    }
    else if(PRTE_FLAG_TEST(pset, PRTE_PSET_FLAG_SUB)){
        return PMIX_PSETOP_SUB;
    }
    else if(PRTE_FLAG_TEST(pset, PRTE_PSET_FLAG_REPLACE)){
        return PMIX_PSETOP_REPLACE;
    }
    else if(PRTE_FLAG_TEST(pset, PRTE_PSET_FLAG_UNION)){
        return PMIX_PSETOP_UNION;
    }
    else if(PRTE_FLAG_TEST(pset, PRTE_PSET_FLAG_DIFFERENCE)){
        return PMIX_PSETOP_DIFFERENCE;
    }
    else if(PRTE_FLAG_TEST(pset, PRTE_PSET_FLAG_INTERSECTION)){
        return PMIX_PSETOP_INTERSECTION;
    }
    else if(PRTE_FLAG_TEST(pset, PRTE_PSET_FLAG_DEFINE)){
        return PMIX_PSETOP_DEFINE;
    }
    else if(PRTE_FLAG_TEST(pset, PRTE_PSET_FLAG_SPLIT)){
        return PMIX_PSETOP_SPLIT;
    }

    return PMIX_PSETOP_NULL;
}

static void prte_pset_set_flags(prte_pset_t *pset, pmix_alloc_directive_t op){
    switch(op){
        case PMIX_PSETOP_NULL:
            PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_ORIGIN);
            return;
        case PMIX_PSETOP_ADD:
            PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_ADD);
            return;
        case PMIX_PSETOP_SUB:
            PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_SUB);
            return;
        case PMIX_PSETOP_REPLACE:
            PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_REPLACE);
            return;
        case PMIX_PSETOP_UNION:
            PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_UNION);
            return;
        case PMIX_PSETOP_DIFFERENCE:
            PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_DIFFERENCE);
            return;
        case PMIX_PSETOP_INTERSECTION:
            PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_INTERSECTION);
            return;
        case PMIX_PSETOP_DEFINE:
            PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_DEFINE);
            return;
        case PMIX_PSETOP_SPLIT:
            PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_SPLIT);
            return;
    }
}

pmix_status_t prte_pset_define( char * pset_name, pmix_proc_t *procs, size_t nprocs, 
                                pmix_alloc_directive_t op, pmix_nspace_t nspace, size_t index){
    
    prte_pset_t *pset;
    prte_grpcomm_signature_t *sig;
    prte_daemon_cmd_flag_t cmmnd;
    pmix_data_buffer_t *buf;
    pmix_status_t rc;

    pmix_output_verbose(5, prte_setop_server_output, 
        "%s Defining PSet '%s' in nspace '%s' with %ld procs for op %d index %ld",
        PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), pset_name, nspace, nprocs, op, index);
    
    /* add the pset to our server globals */
    if(NULL == (pset = prte_get_pset_object(pset_name))){
        
        pset = PMIX_NEW(prte_pset_t);
        pset->name = strdup(pset_name);
        pset->num_members = nprocs;
        /* Set the range for this PSet to the clients namespace*/
        strcpy(pset->range_specifier, nspace);

        PRTE_FLAG_SET(pset, PRTE_PSET_FLAG_RANGE_NAMESPACE);
        /* Set the flag to indicate the operation that created this PSet */
        if(PMIX_PSETOP_GROW == op){
            prte_pset_set_flags(pset, 0 == index ? PMIX_PSETOP_ADD : PMIX_PSETOP_UNION);
        }else if(PMIX_PSETOP_SHRINK == op){
            prte_pset_set_flags(pset, 0 == index ? PMIX_PSETOP_SUB : PMIX_PSETOP_DIFFERENCE);
        }else if(PMIX_PSETOP_REPLACE == op){
            prte_pset_set_flags(pset, 1 > index ? PMIX_PSETOP_SUB : 2 > index ? PMIX_PSETOP_ADD :  PMIX_PSETOP_REPLACE);
        }else{
            prte_pset_set_flags(pset, op);
        }


        /* insert the membership */
        PMIX_PROC_CREATE(pset->members, pset->num_members);
        memcpy(pset->members, procs, nprocs * sizeof(pmix_proc_t));
        pmix_pointer_array_add(prte_psets, pset);
        /* also pass it down to the pmix_server */
        PMIx_server_define_process_set(procs, nprocs, pset_name);
    }

    // goes to all daemons
    PMIX_DATA_BUFFER_CREATE(buf);
    cmmnd = PRTE_DAEMON_DEFINE_PSET;
    /* pack the command */
    rc = PMIx_Data_pack(NULL, buf, &cmmnd, 1, PMIX_UINT8);
    if (PMIX_SUCCESS != rc) {
        PMIX_ERROR_LOG(rc);
        PMIX_DATA_BUFFER_RELEASE(buf);
        return rc;
    }
    // pack the pset name
    rc = PMIx_Data_pack(NULL, buf, &pset->name, 1, PMIX_STRING);
    if (PMIX_SUCCESS != rc) {
        PMIX_ERROR_LOG(rc);
        PMIX_DATA_BUFFER_RELEASE(buf);
        return rc;
    }
    // pack the #targets
    rc = PMIx_Data_pack(NULL, buf, &pset->num_members, 1, PMIX_INT32);
    if (PMIX_SUCCESS != rc) {
        PMIX_ERROR_LOG(rc);
        PMIX_DATA_BUFFER_RELEASE(buf);
        return rc;
    }
    // pack the targets
    if(0 < pset->num_members){
        rc = PMIx_Data_pack(NULL, buf, (void*)pset->members, pset->num_members, PMIX_PROC);
        if (PMIX_SUCCESS != rc) {
            PMIX_ERROR_LOG(rc);
            PMIX_DATA_BUFFER_RELEASE(buf);
            return rc;
        }
    }
    /* pack the PSet flags */
    if(PMIX_SUCCESS != (rc = PMIx_Data_pack(NULL, buf, &pset->flags, 1, PMIX_UINT16))){
        PMIX_DATA_BUFFER_RELEASE(buf);
        PRTE_ERROR_LOG(rc);
        return rc;
    }
    /* pack the PSet range specifier */
    if(PMIX_SUCCESS != (rc = PMIx_Data_pack(NULL, buf, &pset->range_specifier, 1, PMIX_PROC_NSPACE))){
        PMIX_DATA_BUFFER_RELEASE(buf);
        PRTE_ERROR_LOG(rc);
        return rc;
    }
    /* goes to all daemons */
    sig = PMIX_NEW(prte_grpcomm_signature_t);
    sig->signature = (pmix_proc_t *) malloc(sizeof(pmix_proc_t));
    sig->sz = 1;
    PMIX_LOAD_PROCID(&sig->signature[0], PRTE_PROC_MY_NAME->nspace, PMIX_RANK_WILDCARD);
    if (PRTE_SUCCESS != (rc = prte_grpcomm.xcast(sig, PRTE_RML_TAG_DAEMON, buf))) {
        PRTE_ERROR_LOG(rc);
    }
    PMIX_DATA_BUFFER_RELEASE(buf);
    PMIX_RELEASE(sig);
    if (PMIX_SUCCESS != rc) {
        return rc;
    }
    return PRTE_SUCCESS;
    
}
/******************************
 * PSet Operation Utility 
 ******************************/
static pmix_status_t prte_setops_add_scheduling_decision(prte_setop_t *setops, size_t num_setops, pmix_info_t *info, size_t ninfo){
    int new_col_size;
    size_t n, i;
    pmix_info_t *new_col;

    prte_setop_t *setop;
    pmix_output_verbose(20, prte_setop_server_output, 
        "%s prte_setops_add_scheduling_decision: adding %ld infos to the %ld infos of setop",
        PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), ninfo, setops[0].n_col);
    if(0 == ninfo || NULL == info){
        return PMIX_SUCCESS;
    }

    for(n = 0; n < num_setops; n++){
        setop = &setops[n];
        new_col_size = setop->n_col + ninfo;
        PMIX_INFO_CREATE(new_col, new_col_size);

        /* Copy old COL */
        for(i = 0; i < setop->n_col; i++){
            PMIX_INFO_XFER(&new_col[i], &setop->col[i]);
        }
        /* Add new COL */
        for(i = 0; i < ninfo; i++){
            /* If they provided output names assign them to the PSet Op */
            if(PMIX_CHECK_KEY(&info[i], PMIX_PSETOP_OUTPUT) && 0 == setop->n_output_names){
                setop->output_names = pmix_argv_split_with_empty(info[i].value.data.string, ',');
                setop->n_output_names = pmix_argv_count(setop->output_names);
            }
            PMIX_INFO_XFER(&new_col[setop->n_col + i], &info[i]);
        }
        PMIX_INFO_FREE(setop->col, setop->n_col);
        setop->col = new_col;
        setop->n_col = new_col_size;
    }

    return PMIX_SUCCESS;
}

static pmix_status_t prte_setop_set_default_names(prte_setop_t *setop, size_t n_output, size_t *pset_sizes){
    char *suffix;
    char *tmp;
    int str_len;
    size_t i;

    setop->n_output_names = n_output;
    for(i = 0; i < n_output; i++){

        /* Account for empty set */
        if(0 == pset_sizes[i]){
            PMIX_ARGV_APPEND_NOSIZE_COMPAT(&setop->output_names,"");
        }else{
            str_len = snprintf( NULL, 0, "%d", dummy_name_ctr);
            suffix = (char *) malloc( str_len + 1 );   
            snprintf( suffix, str_len + 1, "%d", dummy_name_ctr);
            dummy_name_ctr++;
            tmp = malloc(str_len + strlen(prte_pset_base_name)+ 1);
            strcpy(tmp, prte_pset_base_name);
            strcat(tmp, suffix);
            free(suffix);

            PMIX_ARGV_APPEND_NOSIZE_COMPAT(&setop->output_names,tmp);
            free(tmp);
        }
        
    }


    return PMIX_SUCCESS;
}

static void do_nothing(pmix_status_t status, void * nothing){
    PRTE_HIDE_UNUSED_PARAMS(status, nothing);
}

static pmix_status_t prte_setop_publish_pset_info(prte_setop_t *setop){
    int rc;
    size_t n;
    size_t ninfo;
    pmix_info_t *info;
    /* publish the PSet data */
    if(0 < setop->nset_info_arrays){
        for(n = 0; n < setop->nset_info_arrays; n++){
            info = (pmix_info_t *) setop->set_info_arrays[n].array;
            ninfo = setop->set_info_arrays[n].size;
            if(0 < ninfo){
                rc = pmix_server_publish_fn(PRTE_PROC_MY_NAME, info, ninfo, do_nothing, NULL);
                if(PMIX_SUCCESS != rc){
                    return rc;
                }
            }
        }
    }
    return PMIX_SUCCESS;    
}

/* Returns a comma seperated list of alloc_ids the given setops depend on - NULL*/
static pmix_status_t prte_setops_check_dependencies(prte_setop_t *setops, size_t num_setops, char **deps){
    size_t i, n, j, k;
    int m;
    char **deps_argv = NULL;
    bool dep_found = false, done;

    pmix_server_req_t * req;
    prte_setop_t *setop, *setop_to_check, *req_setops;

    if(NULL == setops || 0 == num_setops){
        return PMIX_ERR_BAD_PARAM;
    }

    *deps = NULL;
    /* We do not allow any PSetOps that involve PSet from any pending PSetOp */
    for(n = 0; n < num_setops; n++){
        setop_to_check = &setops[n];
        if(PMIX_PSETOP_ADD == setop_to_check->op || PMIX_PSETOP_SUB == setop_to_check->op || 
            PMIX_PSETOP_REPLACE == setop_to_check->op || PMIX_PSETOP_CANCEL == setop_to_check->op ||
            PMIX_PSETOP_GROW == setop_to_check->op  || PMIX_PSETOP_SHRINK == setop_to_check->op ||
            PMIX_PSETOP_UNION == setop_to_check->op || PMIX_PSETOP_DIFFERENCE == setop_to_check->op)
        {   
            for(m = 0; m < prte_pmix_server_globals.local_reqs.size; m++){
                if( NULL == (req = pmix_pointer_array_get_item(&prte_pmix_server_globals.local_reqs, m))
                    || 0 == req->sz){
                    continue;
                }

                req_setops = (prte_setop_t *)req->data;

                dep_found = false;
                done = true;
                for(i = 0; i < req->sz; i++){
                    setop = &req_setops[i];
                    if(setop->state != PRTE_SETOP_STATE_FINALIZED){
                        done = false;
                    }
                    for(k = 0; k < setop_to_check->n_output_names; k++){
                        for(j = 0; j < setop->n_input_names; j++){
                            if(0 == strcmp(setop_to_check->output_names[k], setop->input_names[j])){
                                dep_found = true;
                            }
                        }
                    }
                    for(k = 0; k < setop_to_check->n_input_names; k++){
                        for(j = 0; j < setop->n_output_names; j++){
                            if(0 == strcmp(setop_to_check->input_names[k], setop->output_names[j])){
                                dep_found = true;
                            }
                        }
                    }
                    
                }
                if(!done && dep_found){
                    PMIX_ARGV_APPEND_NOSIZE_COMPAT(&deps_argv, req_setops[0].alloc_id);
                }
            }
        }
    }
    if(deps_argv){
        *deps = PMIX_ARGV_JOIN_COMPAT(deps_argv, ',');
        PMIX_ARGV_FREE_COMPAT(deps_argv);
    }
    return PMIX_SUCCESS;
}

static pmix_status_t prte_setops_validate(prte_setop_t * setops, size_t num_setops, bool empty_nodes){

    pmix_status_t ret;
    int num_procs_add, num_procs_sub, num_nodes_add, num_nodes_sub, num_nodes_added;
    int slots, slots_avail;
    bool found, add_nodes, already_added, coscheduling = false;
    int n, i;
    size_t k;
    pmix_info_t *info;
    prte_setop_t *setop;
    prte_job_t *jdata;
    prte_node_t *node;
    prte_job_map_t *map;
    char ** nodes_add = NULL, **nodes_sub = NULL, **ppr = NULL, *tmp, buf[64];
    char **nodes_added = NULL, **ppr_added = NULL;

    setop = &setops[0];

    pmix_output_verbose(5, prte_setop_server_output, 
            "%s prte_setops_validate: Called for setop of type %d",
            PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), setop->op);

    /* If the setop does not require changing resources we can serve it immediately */
    if( setop->op != PMIX_PSETOP_ADD && setop->op != PMIX_PSETOP_SUB && 
        setop->op != PMIX_PSETOP_GROW && setop->op != PMIX_PSETOP_SHRINK && 
        setop->op != PMIX_PSETOP_REPLACE)
    {        
        return PMIX_SUCCESS;
    }

    num_procs_add = 0;
    num_procs_sub = 0;
    num_nodes_add = 0;
    num_nodes_sub = 0;
    num_nodes_added = 0;

    for(k = 0; k < setop->n_col; k++){
        if(PMIX_CHECK_KEY(&setop->col[k], "mpi_num_procs_add")){
            sscanf(setop->col[k].value.data.string, "%d", &num_procs_add);
        }else if(PMIX_CHECK_KEY(&setop->col[k], "mpi_num_procs_sub")){
            sscanf(setop->col[k].value.data.string, "%d", &num_procs_sub);
        }else if(PMIX_CHECK_KEY(&setop->col[k], PMIX_NODE_LIST)){
            nodes_add = pmix_argv_split(setop->col[k].value.data.string, ',');
            num_nodes_add = pmix_argv_count(nodes_add);
        }else if(PMIX_CHECK_KEY(&setop->col[k], PMIX_PPR)){
            ppr = pmix_argv_split(setop->col[k].value.data.string, ',');
        /**/
        }else if(PMIX_CHECK_KEY(&setop->col[k], PMIX_NODE_LIST_SUB)){
            nodes_sub = pmix_argv_split(setop->col[k].value.data.string, ',');
            num_nodes_sub = pmix_argv_count(nodes_sub);
        } 
    }

    pmix_output_verbose(20, prte_setop_server_output, 
        "%s prte_setops_validate: Setop of type %d: Requested adding %d and removing %d procs",
        PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), setop->op, num_procs_add, num_procs_sub);

    add_nodes = (NULL == nodes_add);

    /* TODO: Do more analysis. E.g. go over all set operations and aggregate the required resources */
    jdata = prte_get_job_data_object(setop->nspace);
    
    /* At least 1 process needs to be left after the operation */
    if( num_procs_sub >= num_procs_add + (int) jdata->num_procs ||
        num_nodes_sub > jdata->map->num_nodes){
        pmix_output_verbose(5, prte_setop_server_output, 
            "%s Requested removing %d procs / %d nodes, but there are only %d procs / %d nodes in this job",
            PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), num_procs_sub, num_procs_sub, (int) jdata->num_procs, (int)jdata->map->num_nodes);        
        ret = PMIX_ERR_OUT_OF_RESOURCE;
        goto DONE;
    }
    
    /* Step 4: If they requested additional resources check if we can provide them from our own node pool 
     * If yes: proceed to execute the res change (alloc_cb). We do not need to communicate with the scheduler
     * If no, possibly reserve some nodes (include reservation number in cbdata) and goto Step 5
     */

    
    /* Count the free slots in the job */
    map = jdata->map;
    slots = 0;
    for (n = 0; n < map->nodes->size && slots < num_procs_add; n++){
        if (NULL == (node = (prte_node_t *) pmix_pointer_array_get_item(map->nodes, n))) {
            continue;
        }

        /* If we coschedule we just fill up the free slots of the nodes even if other jobs are running on them */
        if(coscheduling){
            slots_avail = get_available_slots(node, nodes_add, num_nodes_add, ppr, empty_nodes);
        }else{
            slots_avail = get_available_slots(node, nodes_add, num_nodes_add, ppr, true);
        }
        if(0 == slots_avail){
            continue;
        }

        slots += slots_avail;

        PMIX_ARGV_APPEND_NOSIZE_COMPAT(&nodes_added, node->name);
        snprintf(buf, 64, "%d", slots_avail);
        PMIX_ARGV_APPEND_NOSIZE_COMPAT(&ppr_added, buf);
        num_nodes_added++;
        if(slots >= num_procs_add){
            goto DONE;
        }    
    }
    
    /* Count free slots in the node pool */
    /* TODO: Need to decide how to set this as user/scheduler */
    for (n = 0; n < prte_node_pool->size && slots < num_procs_add; n++) {
        if (NULL == (node = (prte_node_t *) pmix_pointer_array_get_item(prte_node_pool, n))) {
            continue;
        }
        already_added = false;
        for(i = 0; i < num_nodes_added; i++){
            if(0 == strcmp(node->name, nodes_added[i])){
                already_added = true;
            }
        }
        if(already_added){
            continue;
        }

        /* If we coschedule we just fill up the free slots of the nodes even if other jobs are running on them */
        if(coscheduling){
            slots_avail = get_available_slots(node, nodes_add, num_nodes_add, ppr, empty_nodes);
        }else{
            slots_avail = get_available_slots(node, nodes_add, num_nodes_add, ppr, true);
        }
        if(0 == slots_avail){
            continue;
        }
        slots += slots_avail;
        PMIX_ARGV_APPEND_NOSIZE_COMPAT(&nodes_added, node->name);
        snprintf(buf, 64, "%d", slots_avail);
        PMIX_ARGV_APPEND_NOSIZE_COMPAT(&ppr_added, buf);
        num_nodes_added++;
        if(slots >= num_procs_add){
            goto DONE;
        }
    }

    /* Count the slots of any nodes that are added to the node pool */
    for(n = 0; n < num_nodes_add; n++){
        found = false;
        for(i = 0; i < prte_node_pool->size; i++){
            if (NULL == (node = (prte_node_t *) pmix_pointer_array_get_item(prte_node_pool, i))) {
                continue;
            }
            if(node->name == nodes_add[n]){
                found = true;
                break;
            }            
        }
        if(!found){
            slots += atoi(ppr[n]);
        }
    }

DONE:
    if(slots >= num_procs_add){
        ret = PMIX_SUCCESS;
        if(add_nodes){

            PMIX_INFO_CREATE(info, 2);
            
            tmp = PMIX_ARGV_JOIN_COMPAT(nodes_added, ',');
            PMIX_INFO_LOAD(&info[0], PMIX_NODE_LIST, tmp, PMIX_STRING);
            free(tmp);

            tmp = PMIX_ARGV_JOIN_COMPAT(ppr_added, ',');
            PMIX_INFO_LOAD(&info[1], PMIX_PPR, tmp, PMIX_STRING);
            free(tmp);

            prte_setops_add_scheduling_decision(setops, num_setops, info, 2);

            PMIX_INFO_FREE(info, 2);
        }
    }else if(slots > 0){
        pmix_output_verbose(5, prte_setop_server_output, 
            "%s Not enough slots: %d requested but only %d available",
            PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), num_procs_add, slots);
        ret = PMIX_ERR_OUT_OF_RESOURCE;
    }
    pmix_argv_free(nodes_add);
    pmix_argv_free(nodes_sub);
    pmix_argv_free(ppr);

    return ret;
}

pmix_status_t prte_setops_get_ouput_list(prte_setop_t *setops, size_t num_setops, char **outputlist){
    char ** output_argv = NULL;
    size_t n, i;
    for(n = 0; n < num_setops; n++){
        for(i = 0; i < setops[n].n_output_names; i++){
            PMIX_ARGV_APPEND_NOSIZE_COMPAT(&output_argv, setops[n].output_names[i]);
        }
        
    }
    if(NULL == output_argv){
        return PMIX_ERR_BAD_PARAM;
    }

    *outputlist = PMIX_ARGV_JOIN_COMPAT(output_argv, ',');

    return PMIX_SUCCESS;
}

/****************************
 * PSet Operation Creation 
 ****************************/
prte_setop_t * prte_setop_create(  pmix_alloc_directive_t op, char *input_names, 
                        char *output_names, 
                        pmix_info_t *col, size_t n_col, 
                        pmix_value_t *set_infos, size_t n_set_infos)
{
    prte_setop_t *setop;
    prte_pset_t *pset;
    pmix_info_t *info, *info2;
    size_t ninfo;

    size_t n,i;

    if(NULL == input_names){
        return NULL;
    }

    setop = PMIX_NEW(prte_setop_t);
    
    /* Set the Setop type*/
    setop->op = op;

    /* Copy the input names*/
    setop->input_names = PMIX_ARGV_SPLIT_WITH_EMPTY_COMPAT(input_names, ',');
    setop->n_input_names = PMIX_ARGV_COUNT_COMPAT(setop->input_names);
    
    pset = prte_get_pset_object(setop->input_names[0]);
    PMIX_LOAD_NSPACE(setop->nspace, pset->range_specifier);

    /* Copy the output names*/
    if(NULL != output_names){
        setop->output_names = PMIX_ARGV_SPLIT_WITH_EMPTY_COMPAT(output_names, ',');
        setop->n_output_names = PMIX_ARGV_COUNT_COMPAT(setop->output_names);
    }

    /* Transfer the COL infos */
    if(NULL != col && 0 < n_col){
        setop->n_col = n_col;
        PMIX_INFO_CREATE(setop->col, n_col);
        for(n = 0; n < n_col; n++){
            PMIX_INFO_XFER(&setop->col[n], &col[n]);
        }
    }

    /* Transfer the set info arrays */
    if(NULL != set_infos && 0 < n_set_infos){
        setop->nset_info_arrays = n_set_infos;
        setop->set_info_arrays = malloc(n_set_infos * sizeof(pmix_data_array_t *));
        for(n = 0; n < n_set_infos; n++){
            info = (pmix_info_t *) set_infos[n].data.darray->array;
            ninfo = set_infos[n].data.darray->size;
            PMIX_DATA_ARRAY_CONSTRUCT(&setop->set_info_arrays[n], ninfo, PMIX_INFO);
            info2 = (pmix_info_t *) setop->set_info_arrays[n].array;
            for(i = 0; i < ninfo; i++){
                PMIX_INFO_XFER(&info2[i], &info[i]);
            }
        } 
    }

    return setop;
}

static pmix_status_t prte_setop_construct(  pmix_alloc_directive_t op, char *input_names, 
                        char *output_names, 
                        pmix_info_t *col, size_t n_col, 
                        pmix_value_t *set_infos, size_t n_set_infos, prte_setop_t *setop)
{
    prte_pset_t *pset;
    pmix_info_t *info, *info2;
    size_t ninfo;

    size_t n,i;

    if(NULL == input_names){
        return PMIX_ERR_BAD_PARAM;
    }

    PMIX_CONSTRUCT(setop, prte_setop_t);
    
    /* Set the Setop type*/
    setop->op = op;

    /* Copy the input names*/
    setop->input_names = PMIX_ARGV_SPLIT_WITH_EMPTY_COMPAT(input_names, ',');
    setop->n_input_names = PMIX_ARGV_COUNT_COMPAT(setop->input_names);
    pset = prte_get_pset_object(setop->input_names[0]);
    PMIX_LOAD_NSPACE(setop->nspace, pset->range_specifier);

    /* Copy the output names*/
    if(NULL != output_names){
        setop->output_names = PMIX_ARGV_SPLIT_WITH_EMPTY_COMPAT(output_names, ',');
        setop->n_output_names = PMIX_ARGV_COUNT_COMPAT(setop->output_names);
    }
    /* Transfer the COL infos */
    if(NULL != col && 0 < n_col){
        setop->n_col = n_col;
        PMIX_INFO_CREATE(setop->col, n_col);
        for(n = 0; n < n_col; n++){
            PMIX_INFO_XFER(&setop->col[n], &col[n]);
        }
    }
    /* Transfer the set info arrays */
    if(NULL != set_infos && 0 < n_set_infos){
        setop->nset_info_arrays = n_set_infos;
        setop->set_info_arrays = malloc(n_set_infos * sizeof(pmix_data_array_t ));
        for(n = 0; n < n_set_infos; n++){
            info = (pmix_info_t *) set_infos[n].data.darray->array;
            ninfo = set_infos[n].data.darray->size;
            PMIX_DATA_ARRAY_CONSTRUCT(&setop->set_info_arrays[n], ninfo, PMIX_INFO);
            info2 = (pmix_info_t *) setop->set_info_arrays[n].array;
            for(i = 0; i < ninfo; i++){
                PMIX_INFO_XFER(&info2[i], &info[i]);
            }
        } 
    }

    return PMIX_SUCCESS;
}

/* Creates a setop to track the initial job launch */
pmix_status_t prte_setop_create_launch_op(prte_job_t *jdata){
    int n, i = 0;
    pmix_proc_t * procs;
    prte_proc_t *pptr;
    char *pset_name, *setop_id;
    prte_setop_t *setop;
    pmix_server_req_t *req;

    pmix_output_verbose(5, prte_setop_server_output, 
                        "%s prte_setop_create_launch_op: Creating launch setop for job %s",
                        PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), jdata->nspace);

    /* Store the highest rank of the job */
    set_highest_job_rank(jdata->nspace, jdata->num_procs - 1);

    /* Now create the origin PSetOp */
    if (!prte_get_attribute(&jdata->attributes, PRTE_JOB_PSET_NAME, (void **) &pset_name, PMIX_STRING)) {
        pset_name = malloc(strlen(jdata->nspace) + strlen("/launch_set") + 1);
        strcpy(pset_name, jdata->nspace);
        strcat(pset_name, "/launch_set");
    }
    if (!prte_get_attribute(&jdata->attributes, PRTE_JOB_PSETOP_ID, (void **) &setop_id, PMIX_STRING)) {
        setop_id = malloc(strlen(jdata->nspace) + strlen("/launch_setop") + 1);
        strcpy(setop_id, jdata->nspace);
        strcat(setop_id, "/launch_setop");
    }

    if (NULL == (setop = prte_setop_create(PMIX_PSETOP_ADD, "", pset_name, NULL, 0, NULL, 0))){
        PRTE_ERROR_LOG(PRTE_ERR_OUT_OF_RESOURCE);
        PMIX_RELEASE(setop);
        return PRTE_ERR_OUT_OF_RESOURCE;
    }
    setop->alloc_id = strdup(setop_id);
    setop->n_procs_launch = jdata->num_procs;
    free(setop_id);

    procs = malloc(jdata->num_procs * sizeof(pmix_proc_t));
    for(n = 0; n < jdata->procs->size; n++){
        if(NULL == (pptr = (prte_proc_t *) pmix_pointer_array_get_item(jdata->procs, n))){
            continue;
        }
        prte_set_attribute(&pptr->attributes, PRTE_PROC_ADD_SETOP, PRTE_ATTR_LOCAL, setop->alloc_id, PMIX_STRING);
        PMIX_PROC_LOAD(&procs[i++], pptr->name.nspace, pptr->name.rank);
    }

    prte_pset_define(pset_name, procs, jdata->num_procs, PMIX_PSETOP_NULL, jdata->nspace, 0); 
    free(procs);
    free(pset_name); 

    /* add a request to our local request tracker array */
    req = PMIX_NEW(pmix_server_req_t); 
    req->allocdir = PMIX_ALLOC_SETOP;
    req->data = (char *) setop;
    req->sz = 1;
    req->local_index = pmix_pointer_array_add(&prte_pmix_server_globals.local_reqs, req);
    
    /* Launch is pending */
    PRTE_ACTIVATE_SETOP_STATE((prte_setop_t *) req->data, PRTE_SETOP_STATE_INIT);  

    return PRTE_SUCCESS;
}

static int prte_ophandle_get_nth_op(pmix_info_t *rc_handle, size_t index, prte_setop_t *setop){
    size_t n, k, i, ninfo, nsetop_info, ninfo2, n_col = 0, n_set_info_arrays = 0;

    pmix_value_t *set_info_arrays = NULL;
    pmix_info_t *setop_info, *info, *info2, *col = NULL;

    pmix_alloc_directive_t op = PMIX_ALLOC_EXTERNAL;
    char *output_names = NULL, *input_names = NULL;

    for(n = 0; n < rc_handle[0].value.data.darray->size; n++){
        if(0 == index){
            setop_info = (pmix_info_t *) rc_handle[0].value.data.darray->array;
            nsetop_info = rc_handle[0].value.data.darray->size;
        }else{
            info = (pmix_info_t *) rc_handle[0].value.data.darray->array;
            ninfo = rc_handle[0].value.data.darray->size; 

            for(i = 0; i < ninfo; i++){
                if(PMIX_CHECK_KEY(&info[i], "mpi.set_op_handles")){

                    /*set op handles */
                    info2 = (pmix_info_t *) info[i].value.data.darray->array;

                    if(index - 1 >= info[i].value.data.darray->size){
                        return PMIX_ERR_BAD_PARAM;
                    }

                    setop_info = (pmix_info_t *) info2[index - 1].value.data.darray->array;
                    nsetop_info = info2[index - 1].value.data.darray->size;
                }
            }
        }
    }

    for(n = 0; n < nsetop_info; n++){
        
        /* Get the op type */
        if(PMIX_CHECK_KEY(&setop_info[n], PMIX_PSETOP_TYPE)){
            op = setop_info[n].value.data.uint8;
        }else if(PMIX_CHECK_KEY(&setop_info[n], "mpi.op_info")){            
            info2 = (pmix_info_t *) setop_info[n].value.data.darray->array;
            ninfo2 = setop_info[n].value.data.darray->size;
            for(k = 0; k < ninfo2; k++){
                if(PMIX_CHECK_KEY(&info2[k], "mpi.op_info.info")){                   
                    n_col = info2[k].value.data.darray->size;
                    col = (pmix_info_t *) info2[k].value.data.darray->array;
                }
                /* Get the input sets */
                else if(PMIX_CHECK_KEY(&info2[k], "mpi.op_info.input")){                    
                    input_names = info2[k].value.data.string;
                }
                /* Get the output sets */
                else if(PMIX_CHECK_KEY(&info2[k], "mpi.op_info.output")){
                    output_names = info2[k].value.data.string;
                }
                /* Get the set infos */
                else if(PMIX_CHECK_KEY(&info2[k], "mpi.op_info.set_info")){
                    set_info_arrays = (pmix_value_t *) info2[k].value.data.darray->array;
                    n_set_info_arrays = info2[k].value.data.darray->size;
                }
            }
        }
    }
    return prte_setop_construct(op, input_names, output_names, col, n_col, set_info_arrays, n_set_info_arrays, setop);
}

static int prte_ophandle_get_num_ops(pmix_info_t *rc_handle, size_t *num_ops){
    size_t n, i, ninfo;
    
    pmix_info_t  *info;

    for(n = 0; n < rc_handle[0].value.data.darray->size; n++){

        info = (pmix_info_t *) rc_handle[0].value.data.darray->array;
        ninfo = rc_handle[0].value.data.darray->size; 
        for(i = 0; i < ninfo; i++){
            if(PMIX_CHECK_KEY(&info[i], "mpi.set_op_handles")){
                *num_ops = 1 + info[i].value.data.darray->size;
                return PMIX_SUCCESS;
            }
        }        
    }
    return PMIX_ERR_BAD_PARAM;
}


/* Creates an array of prte_setop_t from PMIx_Allocation_request infos */
static int prte_setops_create(pmix_info_t *info, size_t ninfo, prte_setop_t **setops, size_t * num_setops){
    size_t n, i, _num_setops, ncol = 0;
    pmix_info_t *col = NULL, *rc_op_handle;
    pmix_alloc_directive_t op = PMIX_PSETOP_EXTERNAL;
    pmix_status_t rc;

    char *output_names = NULL, *input_names = NULL;

    rc_op_handle = NULL;
    for(n = 0; n < ninfo; n++){
        if(PMIX_CHECK_KEY(&info[n], "mpi.rc_op_handle")){
            rc_op_handle = &info[n];
            prte_ophandle_get_num_ops(rc_op_handle, &_num_setops);
        }        
    }

    /* They've sent a single PSetOp */
    if(NULL == rc_op_handle){
        for(n = 0; n < ninfo; n++){
            if(PMIX_CHECK_KEY(&info[n], PMIX_PSETOP_TYPE)){
                op = info[n].value.data.adir;
            }else if(PMIX_CHECK_KEY(&info[n], PMIX_PSETOP_INPUT)){
                input_names = info[n].value.data.string;
            }else if(PMIX_CHECK_KEY(&info[n], PMIX_PSETOP_OUTPUT)){
                output_names = info[n].value.data.string;
            }else if(PMIX_CHECK_KEY(&info[n], PMIX_PSETOP_COL)){
                col = info[n].value.data.darray->array;
                ncol = info[n].value.data.darray->size;
            }        
        }
        if(op != PMIX_PSETOP_EXTERNAL && input_names != NULL){
            *setops = malloc(sizeof(prte_setop_t));
            if(PMIX_SUCCESS != (rc = prte_setop_construct(op, input_names, output_names, col, ncol, NULL, 0, &(*setops)[0]))){
                PRTE_ERROR_LOG(rc);
                free(*setops);
                return rc;
            }
            *num_setops = 1;
            return PMIX_SUCCESS;
        }
        return PMIX_ERR_BAD_PARAM;
    }else{
        /* They've sent multiple setops serialized as an mpi_rc_op_handle */
        *setops = (prte_setop_t *) malloc(_num_setops * sizeof(prte_setop_t));
        for(n = 0; n < _num_setops; n++){
            printf("Op %ld\n", n);
            if(PMIX_SUCCESS != (rc = prte_ophandle_get_nth_op(rc_op_handle, n, &(*setops)[n]))){
                for(i = 0; i < n; i++){
                    PMIX_DESTRUCT(&(*setops)[i]);
                }
                PRTE_ERROR_LOG(rc);
                free(*setops);
                return rc;
            }
        }
        *num_setops = _num_setops;
        return PMIX_SUCCESS;
    }
}

static void prte_setops_free( prte_setop_t *setops, size_t num_setops){
    size_t n;

    if(NULL == setops || 0 == num_setops){
        return;
    }

    for(n = 0; n < num_setops; n++){
        PMIX_DESTRUCT(&setops[n]);
    }
    free(setops);
}
/****************************
 * PSet Operation Execution 
 ****************************/
static pmix_status_t psetop_intersection(prte_pset_t **psets, size_t npsets, pmix_proc_t **result, size_t *nmembers){
    size_t n, k, i;
    size_t res_ptr = 0;
    size_t nprocs_max = 0;

    for(n = 0; n < npsets; n++){
        nprocs_max = MAX(nprocs_max, psets[n]->num_members); 
    }
    *result = (pmix_proc_t *) malloc(nprocs_max * sizeof(pmix_proc_t));

    for(n = 0; n < psets[0]->num_members; n++){
        for(i = 1; i < npsets; i++){
            int found=0;
            for(k = 0; k < psets[i]->num_members; k++){
                found += PMIX_CHECK_NAMES(&psets[i]->members[k], &psets[0]->members[n]);
                if(0 < found){
                    break;
                }
            }
            if(0 != found){
                PMIX_PROC_LOAD(&(*result)[res_ptr], psets[0]->members[n].nspace, psets[0]->members[n].rank);
                res_ptr++;
            }
        }
    }
    *nmembers=res_ptr;
    *result = realloc(*result, *nmembers * sizeof(pmix_proc_t));

    return PMIX_SUCCESS;

}

static pmix_status_t psetop_difference(prte_pset_t **psets, size_t npsets, pmix_proc_t **result, size_t *nmembers){
    size_t n, k, i;
    size_t res_ptr = 0;
    size_t nprocs_max = 0;

    /* Allocate enough memory for worst case */
    for(n = 0; n < npsets; n++){
        nprocs_max = MAX(nprocs_max, psets[n]->num_members); 
    }
    *result = (pmix_proc_t *) malloc(nprocs_max * sizeof(pmix_proc_t));

    /* Fill in the procs */
    for(n = 0; n < psets[0]->num_members; n++){
        for(i = 1; i < npsets; i++){
            int found=0;
            for(k = 0; k < psets[i]->num_members; k++){
                found += PMIX_CHECK_NAMES(&psets[i]->members[k], &psets[0]->members[n]);
                if(0 < found){
                    break;
                }
            }
            if(0 == found){
                PMIX_PROC_LOAD(&(*result)[res_ptr], psets[0]->members[n].nspace, psets[0]->members[n].rank);
                res_ptr++;
            }
        }
    }
    *nmembers = res_ptr;

    /* Realloc to actual size */
    *result = realloc(*result, *nmembers * sizeof(pmix_proc_t));

    return PMIX_SUCCESS;

}

static pmix_status_t psetop_union(prte_pset_t **psets, size_t npsets, pmix_proc_t **result, size_t *nmembers){
    size_t n, k, i;
    size_t res_ptr = 0;
    size_t nprocs_max = 0;

    for(n = 0; n < npsets; n++){
        nprocs_max += psets[n]->num_members; 
    }
    *result = (pmix_proc_t *) malloc(nprocs_max * sizeof(pmix_proc_t));

    /* fill in all procs from p1 */
    for(n = 0; n < psets[0]->num_members; n++){
        PMIX_PROC_LOAD(&(*result)[res_ptr], psets[0]->members[n].nspace, psets[0]->members[n].rank);
        res_ptr++;
    }
    for(i = 1; i < npsets; i++){
        /* Greedily fill in all procs from p2 which are not in p1 (b.c. procs from p1 were already added) */
        for(n = 0; n < psets[i]->num_members; n++){
            int found=0;
            for(k = 0; k < res_ptr; k++){
                found += PMIX_CHECK_NAMES(&(*result)[k], &psets[i]->members[n]);
                if(0 < found){
                    break;
                }
            }
            if(0 == found){
                PMIX_PROC_LOAD(&(*result)[res_ptr], psets[i]->members[n].nspace, psets[i]->members[n].rank);
                res_ptr++;
            }
        }
    }
    *nmembers = res_ptr;
    *result = realloc(*result, *nmembers * sizeof(pmix_proc_t));

    return PMIX_SUCCESS;

}

static pmix_status_t psetop_add(pmix_nspace_t nspace, prte_pset_t **psets, size_t npsets, pmix_info_t *info, size_t ninfo, pmix_proc_t **result, size_t *nmembers){
    size_t n;
    size_t num_procs = 0;
    pmix_status_t rc;
    pmix_rank_t highest_rank = 0;

    PRTE_HIDE_UNUSED_PARAMS(psets, npsets);

    for(n = 0; n < ninfo; n++){
        if(PMIX_CHECK_KEY(&info[n], "mpi_num_procs_add")){
            sscanf(info[n].value.data.string, "%zu", &num_procs);
            break;
        }
    }

    rc = get_highest_job_rank(nspace, &highest_rank);
    if(PMIX_SUCCESS != rc){
        PRTE_ERROR_LOG(rc);
        return rc;
    }

    *result = (pmix_proc_t *) malloc(num_procs * sizeof(pmix_proc_t));
    *nmembers = num_procs;

    if(0 == num_procs){
        return PMIX_SUCCESS;
    }

    pmix_rank_t offset = 1;
    for(n = 0; n < num_procs; n++, offset++){
        PMIX_PROC_LOAD(&(*result)[n], nspace, highest_rank + offset);
    }
    rc = set_highest_job_rank(nspace, highest_rank + num_procs);

    return rc;
}

static pmix_status_t psetop_sub(prte_pset_t **psets, size_t npsets, pmix_info_t *info, size_t ninfo, pmix_proc_t **result, size_t *nmembers){
    size_t n, u, num_spec_nodes=0, index = 0, num_procs = 0;
    int i, j;
    bool found;
    char ** nodes = NULL;
    prte_proc_t *proc;

    if(npsets == 0 || NULL == psets){
        PRTE_ERROR_LOG(PRTE_ERR_BAD_PARAM);
        return PRTE_ERR_BAD_PARAM;
    }

    for(n = 0; n < ninfo; n++){
        if(PMIX_CHECK_KEY(&info[n], "mpi_num_procs_sub")){
            sscanf(info[n].value.data.string, "%zu", &num_procs);
            break;
        }
        /* Did they specify specific nodes? */
        else if(PMIX_CHECK_KEY(&info[n], PMIX_NODE_LIST_SUB)){
            nodes = pmix_argv_split(info[n].value.data.string, ',');
            num_spec_nodes = pmix_argv_count(nodes);
        } 
    }

    /* Nothing to do */
    if(0 == num_procs){
        *result = NULL;
        *nmembers = 0;
        return PMIX_SUCCESS;
    }

    *result = (pmix_proc_t *) malloc(num_procs * sizeof(pmix_proc_t));
    *nmembers = num_procs;

    for(j = (int) npsets - 1; j >= 0; j--){
        for(i = psets[j]->num_members - 1; i >= 0;  i--){
            found = false;
            for(u = 0; u < index; u++){
                if(PMIX_CHECK_PROCID(&(*result)[u], &psets[j]->members[i])){
                    found = true;
                    break;
                }
            }
            if(!found){
                if(0 < num_spec_nodes){
                    proc = (prte_proc_t *) prte_get_proc_object_by_rank(prte_get_job_data_object(psets[j]->members[i].nspace), psets[j]->members[i].rank);
                    if(NULL != proc){
                        for(u = 0; u < num_spec_nodes; u++){
                            if(0 == strcmp(proc->node->name, nodes[u])){
                                found = true;
                            }
                        }
                    }
                    if(!found){
                        continue;
                    }
                }
                PMIX_PROC_LOAD(&(*result)[index], psets[j]->members[i].nspace, psets[j]->members[i].rank);
                index ++;
                if(index == num_procs){
                    return PMIX_SUCCESS;
                }
            }
        }
    }
 
    free(*result);
    return PMIX_ERR_BAD_PARAM;
}

static pmix_status_t psetop_replace(prte_pset_t *pset, pmix_proc_t *sub_procs, size_t n_sub, pmix_proc_t *add_procs, size_t n_add, pmix_proc_t **result, size_t *nmembers){

    size_t n, k, index;
    pmix_proc_t * proc1, *proc2;
    bool found;

    *nmembers = pset->num_members + n_add - n_sub; 
    *result = (pmix_proc_t *) malloc(*nmembers * sizeof(pmix_proc_t));

    index = 0;
    for(n = 0; n < pset->num_members; n++) {
        proc1 = &pset->members[n];
        found = false;
        
        for(k = 0; k < n_sub; k++){
            proc2 = &sub_procs[k];
            if(PMIX_CHECK_PROCID(proc1, proc2)){         
                found = true;
                break;
            }
        }

        if(!found){
            PMIX_PROC_LOAD(&(*result)[index], proc1->nspace, proc1->rank);
            index++;
        }
    }

    for(n = 0; n < n_add; n++){
        PMIX_PROC_LOAD(&(*result)[index], add_procs[n].nspace, add_procs[n].rank);
        index++;
    }

    return PMIX_SUCCESS;

}

static pmix_status_t psetop_define(pmix_info_t *info, size_t ninfo, pmix_proc_t **result, size_t *nmembers){
    size_t n, nprocs;
    pmix_proc_t *procs = NULL;

    for(n = 0; n < ninfo; n++){
        if(PMIX_CHECK_KEY(&info[n], PMIX_PSET_MEMBERS)) {
            procs = (pmix_proc_t*) info[n].value.data.darray->array;
            nprocs = info[n].value.data.darray->size;
        }
    }

    if(NULL == procs){
        return PMIX_ERR_BAD_PARAM;
    }

    *nmembers = nprocs;
    *result = (pmix_proc_t *) malloc(nprocs * sizeof(pmix_proc_t));
    for(n = 0; n < nprocs; n++){
        PMIX_PROC_LOAD(&(*result)[n], procs[n].nspace, procs[n].rank);
    }

    return PMIX_SUCCESS;
}

static pmix_status_t psetop_split(prte_setop_t *setop, pmix_info_t *info, size_t ninfo, size_t *noutput, pmix_proc_t ***result, size_t **nmembers){
    size_t n, m, index, total_size = 0;
    int i_part_size;
    pmix_proc_t **procs = NULL;
    prte_pset_t * input_pset = NULL;
    char ** c_part_sizes;
    size_t *_nmembers;

    /* Get the partition sizes as argv array */
    for(n = 0; n < ninfo; n++){
        if(PMIX_CHECK_KEY(&info[n], "mpi_part_sizes")) {
            c_part_sizes = pmix_argv_split(info[n].value.data.string, ',');
        }
    }
    if(NULL == c_part_sizes){
        return PMIX_ERR_BAD_PARAM;
    }

    /* Get the number of partitions*/
    if (0 >= (*noutput = pmix_argv_count(c_part_sizes))){
        pmix_argv_free(c_part_sizes);
        return PMIX_ERR_BAD_PARAM;
    }

    /* Get the input Pset*/
    if(setop->n_input_names > 0){
        input_pset = prte_get_pset_object(setop->input_names[0]);
    }
    if(NULL == input_pset){
        pmix_argv_free(c_part_sizes);
        return PMIX_ERR_BAD_PARAM;
    }


    /* Get the partition/ output_pset sizes */
    *nmembers = malloc(*noutput * sizeof(size_t));
    _nmembers = *nmembers;

    for(n = 0; n < *noutput; n++){
       i_part_size = atoi(c_part_sizes[n]);

       if(0 > i_part_size){
            pmix_argv_free(c_part_sizes);
            free(*nmembers);
            *nmembers = NULL;
            return PMIX_ERR_BAD_PARAM;
       }

       _nmembers[n] = (size_t) i_part_size;
       total_size += _nmembers[n];
    }

    if(total_size > input_pset->num_members){
        pmix_argv_free(c_part_sizes);
        free(*nmembers);
        *nmembers = NULL;
        return PMIX_ERR_BAD_PARAM;
    }

    
    /* Create the member arrays */
    *result = malloc(*noutput * sizeof(pmix_proc_t *));
    procs = *result;

    index = 0;
    for(n = 0; n < *noutput; n++){
        procs[n] = (pmix_proc_t *) malloc(_nmembers[n] * sizeof(pmix_proc_t));
        for(m = 0; m < _nmembers[n]; m++){
            PMIX_PROC_LOAD(&procs[n][m], input_pset->members[index].nspace, input_pset->members[index].rank);
            index++;
        }
    }    

    pmix_argv_free(c_part_sizes);
    return PMIX_SUCCESS;
}

static pmix_status_t prte_setop_create_ouput_psets(prte_setop_t *setop, size_t *noutput, pmix_proc_t ***result, size_t **nmembers){
    
    pmix_status_t ret;
    size_t n;
    prte_pset_t *pset_list_iter;
    prte_pset_t **psets;
    
    /* Lookup the psets in our pset list */
    psets = malloc(setop->n_input_names * sizeof(prte_pset_t *));
    for(n = 0; n < setop->n_input_names; n++){
        /* Lookup if the specified psets exist */
        psets[n] = prte_get_pset_object(setop->input_names[n]);
        /* Pset not found */
        if(NULL == psets[n]){
            PRTE_ERROR_LOG(PMIX_ERR_BAD_PARAM);
            free(psets);
            return PMIX_ERR_BAD_PARAM;
        }
    }
    ret = PMIX_SUCCESS;

    /* Execute the operation */    
    switch(setop->op){
        case PMIX_PSETOP_ADD:{
            *noutput = 1;
            *result = (pmix_proc_t **) malloc(*noutput * sizeof(pmix_proc_t *));
            *nmembers = malloc(*noutput * sizeof(size_t));
            ret = psetop_add(setop->nspace, psets, setop->n_input_names, setop->col, setop->n_col, &(*result)[0], *nmembers);
            break;
        }
        case PMIX_PSETOP_SUB:{
            *noutput = 1;
            *result = (pmix_proc_t **) malloc(*noutput * sizeof(pmix_proc_t *));
            *nmembers = malloc(*noutput * sizeof(size_t));
            ret = psetop_sub(psets, setop->n_input_names, setop->col, setop->n_col, &(*result)[0], *nmembers);
            break;
        }
        case PMIX_PSETOP_GROW:{
            *noutput = 2;
            *result = (pmix_proc_t **) malloc(*noutput * sizeof(pmix_proc_t *));
            *nmembers = malloc(*noutput * sizeof(size_t));
            ret = psetop_add(setop->nspace, psets, setop->n_input_names, setop->col, setop->n_col, &(*result)[0], &(*nmembers)[0]);
            if(PMIX_SUCCESS != ret){
                break;
            }

            pset_list_iter = PMIX_NEW(prte_pset_t);
            pset_list_iter->members = (*result[0]);
            pset_list_iter->num_members = (*nmembers)[0];

            psets = realloc(psets, 2 * sizeof(prte_pset_t *));
            psets[1] = pset_list_iter;
            ret = psetop_union(psets, setop->n_input_names + 1, &(*result)[1], &(*nmembers)[1]);
            pset_list_iter->members = NULL;
            PMIX_RELEASE(pset_list_iter);

            break;
        }
        case PMIX_PSETOP_SHRINK:{
            *noutput = 2;
            *result = (pmix_proc_t **) malloc(*noutput * sizeof(pmix_proc_t *));
            *nmembers = malloc(*noutput * sizeof(size_t));
            ret = psetop_sub(psets, setop->n_input_names, setop->col, setop->n_col, &(*result)[0], &(*nmembers)[0]);
            if(PMIX_SUCCESS != ret){
                break;
            }

            
            pset_list_iter = PMIX_NEW(prte_pset_t);
            pset_list_iter->members = (*result[0]);
            pset_list_iter->num_members = (*nmembers)[0];

            psets = realloc(psets, 2 * sizeof(prte_pset_t *));
            psets[1] = pset_list_iter;
            ret = psetop_difference(psets, 2, &(*result)[1], &(*nmembers)[1]);
            pset_list_iter->members = NULL;
            PMIX_RELEASE(pset_list_iter);

            break;
        }
        case PMIX_PSETOP_REPLACE:{
            *noutput = 3;
            *result = (pmix_proc_t **) malloc(*noutput * sizeof(pmix_proc_t *));
            *nmembers = malloc(*noutput * sizeof(size_t));

            ret = psetop_sub(psets, setop->n_input_names, setop->col, setop->n_col, &(*result)[0], &(*nmembers)[0]);
            if(PMIX_SUCCESS != ret){
                break;
            }
            ret = psetop_add(setop->nspace, psets, setop->n_input_names, setop->col, setop->n_col, &(*result)[1], &(*nmembers)[1]);
            if(PMIX_SUCCESS != ret){
                break;
            }
            ret = psetop_replace(psets[0], (*result)[0], (*nmembers)[0], (*result)[1], (*nmembers)[1], &(*result)[2], &(*nmembers)[2]);
            break;
        }
        case PMIX_PSETOP_UNION: {
            *noutput = 1;
            *result = (pmix_proc_t **) malloc(*noutput * sizeof(pmix_proc_t *));
            *nmembers = malloc(*noutput * sizeof(size_t));
            ret = psetop_union(psets, setop->n_input_names, &(*result)[0], *nmembers);
            break;
        }
        case PMIX_PSETOP_DIFFERENCE:{
            *noutput = 1;
            *result = (pmix_proc_t **) malloc(*noutput * sizeof(pmix_proc_t *));
            *nmembers = malloc(*noutput * sizeof(size_t));
            ret = psetop_difference(psets, setop->n_input_names, &(*result)[0], *nmembers);
            break;
        }
        case PMIX_PSETOP_INTERSECTION:{
            *noutput = 1;
            *result = (pmix_proc_t **) malloc(*noutput * sizeof(pmix_proc_t *));
            *nmembers = malloc(*noutput * sizeof(size_t));
            ret = psetop_intersection(psets, setop->n_input_names, &(*result)[0], *nmembers);
            break;
        }
        case PMIX_PSETOP_DEFINE:{
            *noutput = 1;
            *result = (pmix_proc_t **) malloc(*noutput * sizeof(pmix_proc_t *));
            *nmembers = malloc(*noutput * sizeof(size_t));
            ret = psetop_define(setop->col, setop->n_col, &(*result)[0], *nmembers);
            break;
        }
        case PMIX_PSETOP_SPLIT:{
            ret = psetop_split(setop, setop->col, setop->n_col, noutput, result, nmembers);
            break;
        }
        default:
            printf("Unknown op: %d\n", setop->op); 
            ret = PMIX_ERR_BAD_PARAM;
    }

    if(PMIX_SUCCESS != ret){
        if(NULL != *result){
            free(*result);
        }
        if(NULL != *nmembers){
            free(*nmembers);
        }
    }

    free(psets);

    return ret;
}

static pmix_status_t prte_setops_define_output_psets(prte_setop_t *setops, size_t num_setops, size_t op_index_start, size_t *op_index_end){
    size_t n;
    int ret;

    size_t n_op_output;
    
    size_t *pset_sizes;
    pmix_proc_t **member_arrays = NULL;

    prte_setop_t *setop;

    *op_index_end = op_index_start;
    for(; *op_index_end < num_setops; (*op_index_end)++){

        setop = &setops[*op_index_end];
        /* noop */
        if(PMIX_PSETOP_NULL == setop->op || PMIX_PSETOP_CANCEL == setop->op){
            continue;
        }

        /* Execute the specified operation */
        n_op_output = 0;
        ret = prte_setop_create_ouput_psets(setop, &n_op_output, &member_arrays, &pset_sizes);
        if(PMIX_SUCCESS != ret){
            PRTE_ERROR_LOG(ret);
            goto ERROR;
        }

        /* Assign output names if none were given */
        if(0 == setop->n_output_names){
            if(PMIX_SUCCESS != (ret = prte_setop_set_default_names(setop, n_op_output, pset_sizes))){
                PRTE_ERROR_LOG(ret);
                goto ERROR;
            }
        }
        
        /* define any new PSets across the DVM */
        for(n = 0; n < n_op_output; n++){
            if(PMIX_SUCCESS != (ret = prte_pset_define(setop->output_names[n], member_arrays[n], pset_sizes[n], setop->op, setop->nspace, n))){
                PRTE_ERROR_LOG(ret);
                goto ERROR;
            }
        }

        if(PMIX_SUCCESS != (ret = prte_setop_publish_pset_info(setop))){
                PRTE_ERROR_LOG(ret);
                goto ERROR;
        }

        for(n = 0; n < n_op_output; n++){
            free(member_arrays[n]);
        }
        free(member_arrays);
        free(pset_sizes);
    }

    return PMIX_SUCCESS;

ERROR:
    for(n = 0; n < n_op_output; n++){
        free(member_arrays[n]);
    }
    free(member_arrays);
    free(pset_sizes);
    return ret;

} 


/****************************
 * SETOP REQUEST PROCESSING *
*****************************/
static void prte_setop_alloc_request_apply(int sd, short args, void *cbdata)
{
    pmix_server_req_t *req;
    prte_setop_t *setops;
    pmix_status_t rc;
    size_t n, num_setops, end_index;

    PRTE_HIDE_UNUSED_PARAMS(sd, args);
    
    req = (pmix_server_req_t *) cbdata;
    setops = (prte_setop_t *) req->data;
    num_setops = req->sz;

    pmix_output_verbose(5, prte_setop_server_output, 
                    "%s prte_setop_alloc_request_apply: called with %ld setops and %ld infos",
                    PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), num_setops, req->ninfo);

    /* bozo check */
    if(1 > num_setops){
        PRTE_ERROR_LOG(PMIX_ERR_BAD_PARAM);
        if(NULL != req->infocbfunc){
            req->infocbfunc(PMIX_ERR_BAD_PARAM, NULL, 0, req->cbdata, NULL, NULL);
        }
        return;
    }

    /* Update the setops with the scheduling decision */
    if(PMIX_SUCCESS != (rc = prte_setops_add_scheduling_decision(setops, num_setops, req->info, req->ninfo))){
        PRTE_ERROR_LOG(rc);
        if(NULL != req->infocbfunc){
            req->infocbfunc(rc, NULL, 0, req->cbdata, NULL, NULL);
        }
        return;
    }
    PMIX_INFO_FREE(req->info, req->ninfo);
    req->ninfo = 0;
    
    /* Check dependencies and delay request if required */
    if(PMIX_SUCCESS != (rc = prte_setops_check_dependencies(setops, num_setops, &req->cmdline))){
        PRTE_ERROR_LOG(rc);
        if(NULL != req->infocbfunc){
            req->infocbfunc(rc, NULL, 0, req->cbdata, NULL, NULL);
        }
        return;
    }
    if(NULL != req->cmdline){
        pmix_output_verbose(10, prte_setop_server_output, 
                "%s prte_setop_alloc_request_apply: Delaying request with alloc_ids %s-%s until setops %s are finalized",
                PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), setops[0].alloc_id, setops[num_setops-1].alloc_id, req->cmdline);
        prte_setop_server_delay_request(req);
        return;
    }

    /* Check if these setops can be applied */
    if(PMIX_SUCCESS != (rc = prte_setops_validate(setops, num_setops, true))){
        if(rc != PMIX_ERR_OUT_OF_RESOURCE){
            PRTE_ERROR_LOG(rc);
        }
        if(NULL != req->infocbfunc){
            req->infocbfunc(rc, NULL, 0, req->cbdata, NULL, NULL);
        }
        return;
    }

    /* Execute the set operations: Creates the new PSets */
    rc = prte_setops_define_output_psets(setops, num_setops, 0, &end_index);
    if(rc != PMIX_SUCCESS){
        PRTE_ERROR_LOG(rc);
        if(NULL != req->infocbfunc){
            req->infocbfunc(rc, NULL, 0, req->cbdata, NULL, NULL);
        }
        return;
    }
    
    /* add this request to our local request tracker array */
    req->local_index = pmix_pointer_array_add(&prte_pmix_server_globals.local_reqs, req);

    /* Start the state machine of the setops */
    for(n = 0; n < num_setops; n++){
        PRTE_ACTIVATE_SETOP_STATE(&setops[n], PRTE_SETOP_STATE_INIT);
    }
}

void prte_setop_alloc_request_cbfunc(pmix_status_t status, pmix_info_t info[], size_t ninfo, void *cbdata, pmix_release_cbfunc_t release_fn, void *release_cbdata){

    pmix_server_req_t *req = (pmix_server_req_t *) cbdata;
    prte_setop_t *setops = (prte_setop_t *) req->data;
    size_t num_setops = req->sz;

    char ** alloc_ids = NULL; 
    size_t n;

    pmix_output_verbose(5, prte_setop_server_output, 
        "%s prte_setop_alloc_request_cbfunc: called with status %d and %ld infos for setop input %s",
        PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), status, ninfo, setops[0].input_names[0]);

    /* If the allocation wasn't successful respond immediately */
    if(PMIX_SUCCESS != status){
        req->infocbfunc(status, NULL, 0, req->cbdata, NULL, NULL);
        if(NULL != release_fn){
            release_fn(release_cbdata);
        }
        return;
    }

    /* Xfer the scheduler results */
    if(0 < ninfo){
        req->ninfo = ninfo;
        PMIX_INFO_CREATE(req->info, ninfo);
        for(n = 0; n < ninfo; n++){
            PMIX_INFO_XFER(&req->info[n], &info[n]);
            if(PMIX_CHECK_KEY(&info[n], PMIX_ALLOC_ID)){
                alloc_ids = PMIX_ARGV_SPLIT_COMPAT(info[n].value.data.string, ',');
            }
        }
        req->copy = true;
    }

    /* The scheduler didn't assign alloc_ids. That's an error! */
    if(NULL == setops[0].alloc_id && (size_t) PMIX_ARGV_COUNT_COMPAT(alloc_ids) != num_setops){
        req->infocbfunc(PMIX_ERR_NOT_FOUND, NULL, 0, req->cbdata, NULL, NULL);
        if(NULL != release_fn){
            release_fn(release_cbdata);
        }
        return;
    }

    /* Assign alloc ids */
    for(n = 0; n < num_setops; n++){
        setops[n].alloc_id = strdup(alloc_ids[n]);
        pmix_output_verbose(10, prte_setop_server_output, 
            "%s prte_setop_alloc_request_cbfunc: Assigned alloc_id %s to setop %ld/%ld in request",
            PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), setops[n].alloc_id, n + 1, num_setops);
    }
    PMIX_ARGV_FREE_COMPAT(alloc_ids); 

    /* Apply the setop alloc request */
    prte_event_set(prte_event_base, &req->ev, -1, PRTE_EV_WRITE, prte_setop_alloc_request_apply, req);
    PMIX_POST_OBJECT(req);
    prte_event_active(&req->ev, PRTE_EV_WRITE, 1);
    
    if(NULL != release_fn){
        release_fn(release_cbdata);
    }
}

pmix_status_t prte_setop_alloc_request_nb(  pmix_alloc_directive_t directive,
                                pmix_info_t *info, size_t ninfo,
                                pmix_info_cbfunc_t cbfunc, void *cbdata){	
    int rc;
    size_t n;
    size_t num_setops;
    pmix_server_req_t *req;
    prte_setop_t *setops;
    char tmp[256];

    pmix_output_verbose(2, prte_setop_server_output, 
                        "%s prte_setop_alloc_request_nb: Received setop alloc request of type %d and %ld infos",
                        PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), directive, ninfo);

    if(!initialized){
        PRTE_ERROR_LOG(PRTE_ERR_NOT_INITIALIZED);
        return PRTE_ERR_NOT_INITIALIZED;
    }

    req = PMIX_NEW(pmix_server_req_t); 
    req->infocbfunc = cbfunc;
    req->cbdata = cbdata;
    req->allocdir = directive;

    /* Create the array of setops. We will apply them later based on the scheduling decision */
    if(PMIX_SUCCESS != (rc = prte_setops_create(info, ninfo, (prte_setop_t **) &req->data, &req->sz))){
        PMIX_RELEASE(req);
        return rc;
    }

    setops = (prte_setop_t *) req->data;
    num_setops = req->sz;

    /* If we have a scheduler connected just forward everything. It will take care of it and call us back if we need to do something */
    if( prte_pmix_server_globals.scheduler_connected){
        pmix_output_verbose(5, prte_setop_server_output, 
                    "%s prte_setop_alloc_request_nb: Forward request type %u and %ld infos to scheduler (input pset = %s)",
                    PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), directive, ninfo, setops[0].input_names[0]);
        
        /* start by ensuring the scheduler is connected to us */
        rc = prte_pmix_set_scheduler();
        if (PMIX_SUCCESS != rc) {
            PRTE_ERROR_LOG(rc);
            prte_setops_free(setops, num_setops);
            PMIX_RELEASE(req);
            return rc;
        }        
        /* pass the request to the scheduler */
        rc = PMIx_Allocation_request_nb(directive, info, ninfo,
                                        prte_setop_alloc_request_cbfunc, req);
        if (PMIX_SUCCESS != rc) {
            PRTE_ERROR_LOG(rc);
            prte_setops_free(setops, num_setops);
            PMIX_RELEASE(req);
        }
        return rc;
        /*
        &&
        NULL != prte_ras_base.active_module && 
        NULL != prte_ras_base.active_module->pmix_allocate){
        
        pmix_output_verbose(5, prte_setop_server_output, 
                    "%s prte_setop_alloc_request_nb: Forward request type %u and %ld infos to scheduler",
                    PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), directive, ninfo);
        if(PRTE_SUCCESS != (rc = prte_ras_base.active_module->pmix_allocate(directive, info, ninfo, prte_setop_alloc_request_cbfunc, req))){
            prte_setops_free((prte_setop_t *) req->data, req->sz);
            PMIX_RELEASE(req);
            return rc;            
        }
        return PMIX_SUCCESS;
        */
    }

    pmix_output_verbose(5, prte_setop_server_output, 
            "%s prte_setop_alloc_request_nb: Process request type %d and %ld infos locally",
            PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), directive, ninfo);
    
    /* Assign alloc ids*/
    for(n = 0; n < num_setops; n++){
        ++alloc_counter;
        snprintf(tmp, 255, "%d", alloc_counter);
        setops[n].alloc_id = strdup(tmp);
        pmix_output_verbose(10, prte_setop_server_output, 
            "%s prte_setop_alloc_request_nb: Assigned alloc_id %s to setop %ld/%ld in request",
            PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), setops[n].alloc_id, n + 1, num_setops);
    }    
    
    prte_event_set(prte_event_base, &req->ev, -1, PRTE_EV_WRITE, prte_setop_alloc_request_apply, req);
    PMIX_POST_OBJECT(req);
    prte_event_active(&req->ev, PRTE_EV_WRITE, 1);
    return PMIX_SUCCESS;
}