/*
 * Copyright (c) 2015-2020 Intel, Inc.  All rights reserved.
 * Copyright (c) 2018-2019 Research Organization for Information Science
 *                         and Technology (RIST).  All rights reserved.
 * Copyright (c) 2020      IBM Corporation.  All rights reserved.
 * Copyright (c) 2020      Cisco Systems, Inc.  All rights reserved
 * Copyright (c) 2021-2024 Nanook Consulting.  All rights reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "prte_config.h"

#include <sys/types.h>
#ifdef HAVE_UNISTD_H
#    include <unistd.h>
#endif /* HAVE_UNISTD_H */
#include <string.h>

#include "src/pmix/pmix-internal.h"
#include "src/prted/pmix/pmix_server.h"
#include "src/prted/pmix/pmix_server_internal.h"
#include "src/util/pmix_argv.h"
#include "src/util/nidmap.h"
#include "src/util/pmix_os_dirpath.h"
#include "src/util/pmix_output.h"
#include "src/util/proc_info.h"
#include "src/util/session_dir.h"

#include "src/mca/errmgr/errmgr.h"
#include "src/mca/filem/filem.h"
#include "src/mca/grpcomm/grpcomm.h"
#include "src/mca/iof/base/base.h"
#include "src/mca/odls/odls_types.h"
#include "src/mca/plm/base/base.h"
#include "src/mca/ras/base/base.h"
#include "src/mca/rmaps/base/base.h"
#include "src/rml/rml.h"
#include "src/runtime/prte_data_server.h"
#include "src/runtime/prte_quit.h"
#include "src/runtime/prte_wait.h"
#include "src/runtime/prte_setop_server.h"
#include "src/threads/pmix_threads.h"

#include "src/mca/state/base/base.h"
#include "state_dvm.h"

/*
 * Module functions: Global
 */
static int init(void);
static int finalize(void);

/* local functions */
static void init_complete(int fd, short args, void *cbdata);
static void vm_ready(int fd, short args, void *cbata);
static void check_complete(int fd, short args, void *cbdata);
static void cleanup_job(int fd, short args, void *cbdata);
static void job_started(int fd, short args, void *cbata);
static void ready_for_debug(int fd, short args, void *cbata);
static void prte_state_setop_init(int sd, short args, void *cbdata);
static void prte_state_setop_mark_sub_procs(int sd, short args, void *cbdata);
static void prte_state_setop_pending(int sd, short args, void *cbdata);
static void prte_state_setop_add_hosts(int sd, short args, void *cbdata);
static void prte_state_setop_add_procs(int sd, short args, void *cbdata);
static void prte_state_setop_sub_procs(int sd, short args, void *cbdata);
static void prte_state_setop_in_finalization(int sd, short args, void *cbdata);
static void prte_state_setop_finalized(int sd, short args, void *cbdata);
static void prte_state_setop_report_error(int sd, short args, void *cbdata);

/******************
 * DVM module - used when mpirun is persistent
 ******************/
prte_state_base_module_t prte_state_dvm_module = {
    .init = init,
    .finalize = finalize,
    .activate_job_state = prte_state_base_activate_job_state,
    .add_job_state = prte_state_base_add_job_state,
    .set_job_state_callback = prte_state_base_set_job_state_callback,
    .remove_job_state = prte_state_base_remove_job_state,
    .activate_proc_state = prte_state_base_activate_proc_state,
    .add_proc_state = prte_state_base_add_proc_state,
    .set_proc_state_callback = prte_state_base_set_proc_state_callback,
    .remove_proc_state = prte_state_base_remove_proc_state,
    .activate_setop_state = prte_state_base_activate_setop_state,
    .add_setop_state = prte_state_base_add_setop_state,
    .set_setop_state_callback = prte_state_base_set_setop_state_callback,
    .remove_setop_state = prte_state_base_remove_setop_state
};

static void dvm_notify(int sd, short args, void *cbdata);

/* defined default state machine sequence - individual
 * plm's must add a state for launching daemons
 */
static prte_job_state_t launch_states[] = {
    PRTE_JOB_STATE_INIT,
    PRTE_JOB_STATE_INIT_COMPLETE,
    PRTE_JOB_STATE_ALLOCATE,
    PRTE_JOB_STATE_ALLOCATION_COMPLETE,
    PRTE_JOB_STATE_DAEMONS_LAUNCHED,
    PRTE_JOB_STATE_DAEMONS_REPORTED,
    PRTE_JOB_STATE_VM_READY,
    PRTE_JOB_STATE_MAP,
    PRTE_JOB_STATE_MAP_COMPLETE,
    PRTE_JOB_STATE_SYSTEM_PREP,
    PRTE_JOB_STATE_LAUNCH_APPS,
    PRTE_JOB_STATE_SEND_LAUNCH_MSG,
    PRTE_JOB_STATE_STARTED,
    PRTE_JOB_STATE_LOCAL_LAUNCH_COMPLETE,
    PRTE_JOB_STATE_READY_FOR_DEBUG,
    PRTE_JOB_STATE_RUNNING,
    PRTE_JOB_STATE_REGISTERED,
    /* termination states */
    PRTE_JOB_STATE_TERMINATED,
    PRTE_JOB_STATE_NOTIFY_COMPLETED,
    PRTE_JOB_STATE_NOTIFIED,
    PRTE_JOB_STATE_ALL_JOBS_COMPLETE
};

static prte_state_cbfunc_t launch_callbacks[] = {
    prte_plm_base_setup_job,
    init_complete,
    prte_ras_base_allocate,
    prte_plm_base_allocation_complete,
    prte_plm_base_daemons_launched,
    prte_plm_base_daemons_reported,
    vm_ready,
    prte_rmaps_base_map_job,
    prte_plm_base_mapping_complete,
    prte_plm_base_complete_setup,
    prte_plm_base_launch_apps,
    prte_plm_base_send_launch_msg,
    job_started,
    prte_state_base_local_launch_complete,
    ready_for_debug,
    prte_plm_base_post_launch,
    prte_plm_base_registered,
    check_complete,
    dvm_notify,
    cleanup_job,
    prte_quit
};

static prte_proc_state_t proc_states[] = {
    PRTE_PROC_STATE_RUNNING,
    PRTE_PROC_STATE_READY_FOR_DEBUG,
    PRTE_PROC_STATE_REGISTERED,
    PRTE_PROC_STATE_IOF_COMPLETE,
    PRTE_PROC_STATE_WAITPID_FIRED,
    PRTE_PROC_STATE_TERMINATED
};

static prte_state_cbfunc_t proc_callbacks[] = {
    prte_state_base_track_procs,
    prte_state_base_track_procs,
    prte_state_base_track_procs,
    prte_state_base_track_procs,
    prte_state_base_track_procs,
    prte_state_base_track_procs
};

static prte_setop_state_t setop_states[] = {
    PRTE_SETOP_STATE_INIT,
    PRTE_SETOP_STATE_MARK_SUB_PROCS,
    PRTE_SETOP_STATE_ADD_HOSTS,
    PRTE_SETOP_STATE_ADD_PROCS,
    PRTE_SETOP_STATE_PENDING,
    PRTE_SETOP_STATE_SUB_PROCS,
    PRTE_SETOP_STATE_IN_FINALIZATION,
    PRTE_SETOP_STATE_FINALIZED
};

static prte_state_cbfunc_t setops_callbacks[] = {
    prte_state_setop_init,
    prte_state_setop_mark_sub_procs,
    prte_state_setop_add_hosts,
    prte_state_setop_add_procs,
    prte_state_setop_pending,
    prte_state_setop_sub_procs,
    prte_state_setop_in_finalization,
    prte_state_setop_finalized
};

static void force_quit(int fd, short args, void *cbdata)
{
    PRTE_HIDE_UNUSED_PARAMS(fd, args);
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;

    /* give us a chance to stop the orteds */
    prte_plm.terminate_orteds();
    PMIX_RELEASE(caddy);
}

/************************
 * Local variables
 ************************/
static bool terminate_dvm = false;
static bool dvm_terminated = false;


/************************
 * API Definitions
 ************************/
static int init(void)
{
    int i, rc;
    int num_states;

    /* setup the state machines */
    PMIX_CONSTRUCT(&prte_job_states, pmix_list_t);
    PMIX_CONSTRUCT(&prte_proc_states, pmix_list_t);
    PMIX_CONSTRUCT(&prte_setop_states, pmix_list_t);

    /* setup the job state machine */
    num_states = sizeof(launch_states) / sizeof(prte_job_state_t);
    for (i = 0; i < num_states; i++) {
        if (PRTE_SUCCESS
            != (rc = prte_state.add_job_state(launch_states[i], launch_callbacks[i]))) {
            PRTE_ERROR_LOG(rc);
        }
    }
    /* add the termination response */
    rc = prte_state.add_job_state(PRTE_JOB_STATE_DAEMONS_TERMINATED, prte_quit);
    if (PRTE_SUCCESS != rc) {
        PRTE_ERROR_LOG(rc);
    }
    /* add a default error response */
    rc = prte_state.add_job_state(PRTE_JOB_STATE_FORCED_EXIT, force_quit);
    if (PRTE_SUCCESS != rc) {
        PRTE_ERROR_LOG(rc);
    }
    /* add callback to report progress, if requested */
    rc = prte_state.add_job_state(PRTE_JOB_STATE_REPORT_PROGRESS,
                                  prte_state_base_report_progress);
    if (PRTE_SUCCESS != rc) {
        PRTE_ERROR_LOG(rc);
    }
    if (5 < pmix_output_get_verbosity(prte_state_base_framework.framework_output)) {
        prte_state_base_print_job_state_machine();
    }

    /* populate the proc state machine to allow us to
     * track proc lifecycle changes
     */
    num_states = sizeof(proc_states) / sizeof(prte_proc_state_t);
    for (i = 0; i < num_states; i++) {
        rc = prte_state.add_proc_state(proc_states[i], proc_callbacks[i]);
        if (PRTE_SUCCESS != rc) {
            PRTE_ERROR_LOG(rc);
        }
    }
    if (5 < pmix_output_get_verbosity(prte_state_base_framework.framework_output)) {
        prte_state_base_print_proc_state_machine();
    }

    /* setup the setop state machine */
    num_states = sizeof(setop_states) / sizeof(prte_setop_state_t);
    for (i = 0; i < num_states; i++) {
        if (PRTE_SUCCESS
            != (rc = prte_state.add_setop_state(setop_states[i], setops_callbacks[i]))) {
            PRTE_ERROR_LOG(rc);
        }
    }
    /* add a default error response */
    rc = prte_state.add_setop_state(PRTE_SETOP_STATE_ERROR, prte_state_setop_report_error);
    if (PRTE_SUCCESS != rc) {
        PRTE_ERROR_LOG(rc);
    }
    if (5 < pmix_output_get_verbosity(prte_state_base_framework.framework_output)) {
        prte_state_base_print_setop_state_machine();
    }

    return PRTE_SUCCESS;
}

static int finalize(void)
{
    /* cleanup the state machines */
    PMIX_LIST_DESTRUCT(&prte_proc_states);
    PMIX_LIST_DESTRUCT(&prte_job_states);
    PMIX_LIST_DESTRUCT(&prte_setop_states);

    return PRTE_SUCCESS;
}

static void files_ready(int status, void *cbdata)
{
    prte_job_t *jdata = (prte_job_t *) cbdata;
    prte_setop_t *setop;

    if (PRTE_SUCCESS != status) {
        PRTE_ACTIVATE_JOB_STATE(jdata, PRTE_JOB_STATE_FILES_POSN_FAILED);
    } else {
        /* Check if we are here due to a set operation 
         * In this case let the setop state machine do the mapping 
         */
        for(int i = 0; i < prte_setops->size; i++){
            if(NULL == (setop = pmix_pointer_array_get_item(prte_setops, i))){
                continue;
            }
            if( 0 == strcmp(setop->nspace, jdata->nspace) &&
                setop->state == PRTE_SETOP_STATE_ADD_HOSTS){
                PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ADD_PROCS); 
                return;  
            }
        }
        PRTE_ACTIVATE_JOB_STATE(jdata, PRTE_JOB_STATE_MAP);
    }
}

static void init_complete(int sd, short args, void *cbdata)
{
    PRTE_HIDE_UNUSED_PARAMS(sd, args);
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;

    PMIX_ACQUIRE_OBJECT(caddy);

    /* need to go thru allocate step in case someone wants to
     * expand the DVM */
    PRTE_ACTIVATE_JOB_STATE(caddy->jdata, PRTE_JOB_STATE_ALLOCATE);
    PMIX_RELEASE(caddy);
}

static void vm_ready(int fd, short args, void *cbdata)
{
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    int rc, i;
    pmix_data_buffer_t buf;
    prte_grpcomm_signature_t sig;
    prte_job_t *jptr;
    prte_proc_t *dmn;
    int32_t v;
    pmix_value_t *val;
    pmix_status_t ret;
    PRTE_HIDE_UNUSED_PARAMS(fd, args, cbdata);

    PMIX_ACQUIRE_OBJECT(caddy);
    /* if this is my job, then we are done */
    if (prte_get_attribute(&caddy->jdata->attributes, PRTE_JOB_LAUNCHED_DAEMONS, NULL, PMIX_BOOL)) {
        /* if there is more than one daemon in the job, then there
         * is just a little bit to do */
        if (!prte_get_attribute(&caddy->jdata->attributes, PRTE_JOB_DO_NOT_LAUNCH, NULL, PMIX_BOOL)
            && 1 < prte_process_info.num_daemons) {
            /* send the daemon map to every daemon in this DVM - we
             * do this here so we don't have to do it for every
             * job we are going to launch */

            PMIX_DATA_BUFFER_CONSTRUCT(&buf);
            rc = prte_util_nidmap_create(prte_node_pool, &buf);
            if (PRTE_SUCCESS != rc) {
                PRTE_ERROR_LOG(rc);
                PMIX_DATA_BUFFER_DESTRUCT(&buf);
                PRTE_ACTIVATE_JOB_STATE(NULL, PRTE_JOB_STATE_FORCED_EXIT);
                return;
            }
            /* get wireup info for daemons */
            jptr = prte_get_job_data_object(PRTE_PROC_MY_NAME->nspace);
            for (v = 0; v < jptr->procs->size; v++) {
                if (NULL == (dmn = (prte_proc_t *) pmix_pointer_array_get_item(jptr->procs, v))) {
                    continue;
                }
                val = NULL;
                if (PMIX_SUCCESS != (ret = PMIx_Get(&dmn->name, PMIX_PROC_URI, NULL, 0, &val)) ||
                    NULL == val) {
                    PMIX_ERROR_LOG(ret);
                    PMIX_DATA_BUFFER_DESTRUCT(&buf);
                    PRTE_ACTIVATE_JOB_STATE(NULL, PRTE_JOB_STATE_FORCED_EXIT);
                    return;
                }
                rc = PMIx_Data_pack(NULL, &buf, &dmn->name, 1, PMIX_PROC);
                if (PMIX_SUCCESS != rc) {
                    PMIX_ERROR_LOG(ret);
                    PMIX_DATA_BUFFER_DESTRUCT(&buf);
                    PRTE_ACTIVATE_JOB_STATE(NULL, PRTE_JOB_STATE_FORCED_EXIT);
                    return;
                }
                rc = PMIx_Data_pack(NULL, &buf, &val->data.string, 1, PMIX_STRING);
                if (PMIX_SUCCESS != rc) {
                    PMIX_ERROR_LOG(ret);
                    PMIX_DATA_BUFFER_DESTRUCT(&buf);
                    PMIX_VALUE_RELEASE(val);
                    PRTE_ACTIVATE_JOB_STATE(NULL, PRTE_JOB_STATE_FORCED_EXIT);
                    return;
                }
                PMIX_VALUE_RELEASE(val);
            }

            /* goes to all daemons */
            PMIX_CONSTRUCT(&sig, prte_grpcomm_signature_t);
            PMIX_PROC_CREATE(sig.signature, 1);
            PMIX_LOAD_PROCID(&sig.signature[0], PRTE_PROC_MY_NAME->nspace, PMIX_RANK_WILDCARD);
            sig.sz = 1;
            if (PRTE_SUCCESS != (rc = prte_grpcomm.xcast(&sig, PRTE_RML_TAG_WIREUP, &buf))) {
                PRTE_ERROR_LOG(rc);
                PMIX_DATA_BUFFER_DESTRUCT(&buf);
                PMIX_PROC_FREE(sig.signature, 1);
                PRTE_ACTIVATE_JOB_STATE(NULL, PRTE_JOB_STATE_FORCED_EXIT);
                return;
            }
            PMIX_DATA_BUFFER_DESTRUCT(&buf);
            PMIX_PROC_FREE(sig.signature, 1);
        }
    }
    if (PMIX_CHECK_NSPACE(PRTE_PROC_MY_NAME->nspace, caddy->jdata->nspace)) {
        prte_dvm_ready = true;
        /* notify that the vm is ready */
        if (0 > prte_state_base.parent_fd) {
            if (prte_state_base.ready_msg && prte_persistent) {
                fprintf(stdout, "DVM ready\n");
                fflush(stdout);
            }
        } else {
            char ok = 'K';
            write(prte_state_base.parent_fd, &ok, 1);
            close(prte_state_base.parent_fd);
            prte_state_base.parent_fd = -1;
        }
        for (i = 0; i < prte_cache->size; i++) {
            jptr = (prte_job_t *) pmix_pointer_array_get_item(prte_cache, i);
            if (NULL != jptr) {
                pmix_pointer_array_set_item(prte_cache, i, NULL);
                prte_plm.spawn(jptr);
            }
        }
        /* progress the job */
        caddy->jdata->state = PRTE_JOB_STATE_VM_READY;
        PMIX_RELEASE(caddy);
        return;
    }

    /* position any required files */
    if (PRTE_SUCCESS != prte_filem.preposition_files(caddy->jdata, files_ready, caddy->jdata)) {
        PRTE_ACTIVATE_JOB_STATE(caddy->jdata, PRTE_JOB_STATE_FILES_POSN_FAILED);
    }
    PMIX_RELEASE(caddy);
}

static void job_started(int fd, short args, void *cbdata)
{
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_job_t *jdata = caddy->jdata;
    pmix_info_t *iptr;
    time_t timestamp;
    pmix_proc_t *nptr;
    PRTE_HIDE_UNUSED_PARAMS(fd, args);

    /* if there is an originator for this job, notify them
     * that the first process of the job has been started */
    if (prte_get_attribute(&jdata->attributes, PRTE_JOB_DVM_JOB, NULL, PMIX_BOOL)) {
        /* dvm job => launch was requested by a TOOL, so we notify the launch proxy
         * and NOT the originator (as that would be us) */
        nptr = NULL;
        if (!prte_get_attribute(&jdata->attributes, PRTE_JOB_LAUNCH_PROXY, (void **) &nptr, PMIX_PROC)
            || NULL == nptr) {
            PRTE_ERROR_LOG(PRTE_ERR_NOT_FOUND);
            return;
        }
        timestamp = time(NULL);
        PMIX_INFO_CREATE(iptr, 5);
        /* target this notification solely to that one tool */
        PMIX_INFO_LOAD(&iptr[0], PMIX_EVENT_CUSTOM_RANGE, nptr, PMIX_PROC);
        PMIX_PROC_RELEASE(nptr);
        /* pass the nspace of the spawned job */
        PMIX_INFO_LOAD(&iptr[1], PMIX_NSPACE, jdata->nspace, PMIX_STRING);
        /* not to be delivered to a default event handler */
        PMIX_INFO_LOAD(&iptr[2], PMIX_EVENT_NON_DEFAULT, NULL, PMIX_BOOL);
        /* provide the timestamp */
        PMIX_INFO_LOAD(&iptr[3], PMIX_EVENT_TIMESTAMP, &timestamp, PMIX_TIME);
        PMIX_INFO_LOAD(&iptr[4], "prte.notify.donotloop", NULL, PMIX_BOOL);
        PMIx_Notify_event(PMIX_EVENT_JOB_START, &prte_process_info.myproc, PMIX_RANGE_CUSTOM, iptr,
                          5, NULL, NULL);
        PMIX_INFO_FREE(iptr, 5);
    }

    PMIX_RELEASE(caddy);
}

static void ready_for_debug(int fd, short args, void *cbdata)
{
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_job_t *jdata = caddy->jdata;
    pmix_proc_t *nptr;
    time_t timestamp;
    pmix_info_t *iptr;
    size_t ninfo;
    pmix_data_array_t darray;
    void *tinfo;
    pmix_status_t rc;
    int n;
    char *name;
    prte_app_context_t *app;
    PRTE_HIDE_UNUSED_PARAMS(fd, args);

    /* launch was requested by a TOOL, so we notify the launch proxy
     * and NOT the originator (as that would be us) */
    nptr = NULL;
    if (!prte_get_attribute(&jdata->attributes, PRTE_JOB_LAUNCH_PROXY, (void **) &nptr, PMIX_PROC)
        || NULL == nptr) {
        PRTE_ERROR_LOG(PRTE_ERR_NOT_FOUND);
        goto DONE;
    }
    timestamp = time(NULL);
    PMIX_INFO_LIST_START(tinfo);
    /* target this notification solely to that one tool */
    PMIX_INFO_LIST_ADD(rc, tinfo, PMIX_EVENT_CUSTOM_RANGE, nptr, PMIX_PROC);
    PMIX_PROC_RELEASE(nptr);
    /* pass the nspace of the job */
    PMIX_INFO_LIST_ADD(rc, tinfo, PMIX_NSPACE, jdata->nspace, PMIX_STRING);
    for (n=0; n < jdata->apps->size; n++) {
        app = (prte_app_context_t *) pmix_pointer_array_get_item(jdata->apps, n);
        if (NULL == app) {
            continue;
        }
        /* if pset name was assigned, pass it */
       if (prte_get_attribute(&app->attributes, PRTE_APP_PSET_NAME, (void**) &name, PMIX_STRING)) {
           PMIX_INFO_LIST_ADD(rc, tinfo, PMIX_PSET_NAME, name, PMIX_STRING);
           free(name);
        }
        /* pass the argv from each app */
        name = PMIX_ARGV_JOIN_COMPAT(app->argv, ' ');
        PMIX_INFO_LIST_ADD(rc, tinfo, PMIX_APP_ARGV, name, PMIX_STRING);
        free(name);
    }

    /* not to be delivered to a default event handler */
    PMIX_INFO_LIST_ADD(rc, tinfo, PMIX_EVENT_NON_DEFAULT, NULL, PMIX_BOOL);
    /* provide the timestamp */
    PMIX_INFO_LIST_ADD(rc, tinfo, PMIX_EVENT_TIMESTAMP, &timestamp, PMIX_TIME);
    PMIX_INFO_LIST_ADD(rc, tinfo, "prte.notify.donotloop", NULL, PMIX_BOOL);
    PMIX_INFO_LIST_CONVERT(rc, tinfo, &darray);
    if (PMIX_ERR_EMPTY == rc) {
        iptr = NULL;
        ninfo = 0;
    } else if (PMIX_SUCCESS != rc) {
        PMIX_ERROR_LOG(rc);
        PRTE_UPDATE_EXIT_STATUS(rc);
        PMIX_INFO_LIST_RELEASE(tinfo);
        PMIX_PROC_RELEASE(nptr);
        goto DONE;
    } else {
        iptr = (pmix_info_t *) darray.array;
        ninfo = darray.size;
    }
    PMIX_INFO_LIST_RELEASE(tinfo);

    PMIx_Notify_event(PMIX_READY_FOR_DEBUG, PRTE_PROC_MY_NAME, PMIX_RANGE_CUSTOM, iptr,
                      ninfo, NULL, NULL);
    PMIX_INFO_FREE(iptr, ninfo);

DONE:
    PMIX_RELEASE(caddy);
}

static void opcbfunc(pmix_status_t status, void *cbdata)
{
    prte_pmix_lock_t *lk = (prte_pmix_lock_t *) cbdata;

    PMIX_POST_OBJECT(lk);
    lk->status = prte_pmix_convert_status(status);
    PRTE_PMIX_WAKEUP_THREAD(lk);
}
static void lkcbfunc(pmix_status_t status, void *cbdata)
{
    prte_pmix_lock_t *lk = (prte_pmix_lock_t *) cbdata;

    PMIX_POST_OBJECT(lk);
    lk->status = prte_pmix_convert_status(status);
    PRTE_PMIX_WAKEUP_THREAD(lk);
}
static void check_complete(int fd, short args, void *cbdata)
{
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_session_t *session;
    prte_job_t *jdata, *jptr;
    prte_proc_t *proc;
    int i, rc;
    prte_node_t *node;
    prte_job_map_t *map;
    int32_t index;
    pmix_proc_t pname;
    prte_pmix_lock_t lock;
    uint8_t command = PRTE_PMIX_PURGE_PROC_CMD;
    pmix_data_buffer_t *buf;
    pmix_pointer_array_t procs;
    prte_timer_t *timer;
    prte_app_context_t *app;
    hwloc_obj_t obj;
    hwloc_obj_type_t type;
    hwloc_cpuset_t boundcpus, tgt;
    bool takeall;
    PRTE_HIDE_UNUSED_PARAMS(fd, args);

    PMIX_ACQUIRE_OBJECT(caddy);
    jdata = caddy->jdata;

    pmix_output_verbose(2, prte_state_base_framework.framework_output,
                        "%s state:dvm:check_job_complete on job %s",
                        PRTE_NAME_PRINT(PRTE_PROC_MY_NAME),
                        (NULL == jdata) ? "NULL" : PRTE_JOBID_PRINT(jdata->nspace));

    if (NULL != jdata &&
        prte_get_attribute(&jdata->attributes, PRTE_JOB_TIMEOUT_EVENT, (void **) &timer, PMIX_POINTER)) {
        /* timer is an prte_timer_t object */
        prte_event_evtimer_del(timer->ev);
        PMIX_RELEASE(timer);
        prte_remove_attribute(&jdata->attributes, PRTE_JOB_TIMEOUT_EVENT);
    }

    if (NULL == jdata || PMIX_CHECK_NSPACE(jdata->nspace, PRTE_PROC_MY_NAME->nspace)) {
        /* just check to see if the daemons are complete */
        PMIX_OUTPUT_VERBOSE(
            (2, prte_state_base_framework.framework_output,
             "%s state:dvm:check_job_complete - received NULL job, checking daemons",
             PRTE_NAME_PRINT(PRTE_PROC_MY_NAME)));
        if (0 == pmix_list_get_size(&prte_rml_base.children)) {
            /* orteds are done! */
            PMIX_OUTPUT_VERBOSE((2, prte_state_base_framework.framework_output,
                                 "%s prteds complete - exiting",
                                 PRTE_NAME_PRINT(PRTE_PROC_MY_NAME)));
            if (NULL == jdata) {
                jdata = prte_get_job_data_object(PRTE_PROC_MY_NAME->nspace);
            }
            PRTE_ACTIVATE_JOB_STATE(jdata, PRTE_JOB_STATE_DAEMONS_TERMINATED);
            PMIX_RELEASE(caddy);
            prte_dvm_ready = false;
            return;
        }
        prte_plm.terminate_orteds();
        PMIX_RELEASE(caddy);
        return;
    }

    /* mark the job as terminated, but don't override any
     * abnormal termination flags
     */
    if (jdata->state < PRTE_JOB_STATE_UNTERMINATED) {
        jdata->state = PRTE_JOB_STATE_TERMINATED;
    }

    /* see if there was any problem */
    if (prte_get_attribute(&jdata->attributes, PRTE_JOB_ABORTED_PROC, NULL, PMIX_POINTER)) {
        rc = prte_pmix_convert_rc(jdata->exit_code);
        /* or whether we got cancelled by the user */
    } else if (prte_get_attribute(&jdata->attributes, PRTE_JOB_CANCELLED, NULL, PMIX_BOOL)) {
        rc = prte_pmix_convert_rc(PRTE_ERR_JOB_CANCELLED);
    } else {
        rc = prte_pmix_convert_rc(jdata->exit_code);
    }

    /* if would be rare, but a very fast terminating job could conceivably
     * reach here prior to the spawn requestor being notified of spawn */
    rc = prte_plm_base_spawn_response(rc, jdata);
    if (PRTE_SUCCESS != rc) {
        PRTE_ERROR_LOG(rc);
    }

    /* cleanup any pending server ops */
    PMIX_LOAD_PROCID(&pname, jdata->nspace, PMIX_RANK_WILDCARD);
    prte_pmix_server_clear(&pname);

    /* cleanup the local procs as these are gone */
    for (i = 0; i < prte_local_children->size; i++) {
        if (NULL == (proc = (prte_proc_t *) pmix_pointer_array_get_item(prte_local_children, i))) {
            continue;
        }
        /* if this child is part of the job... */
        if (PMIX_CHECK_NSPACE(proc->name.nspace, jdata->nspace)) {
            /* clear the entry in the local children */
            pmix_pointer_array_set_item(prte_local_children, i, NULL);
            PMIX_RELEASE(proc); // maintain accounting
        }
    }

    /* tell the IOF that the job is complete */
    if (NULL != prte_iof.complete) {
        prte_iof.complete(jdata);
    }

    /* tell the PMIx subsystem the job is complete */
    PRTE_PMIX_CONSTRUCT_LOCK(&lock);
    PMIx_server_deregister_nspace(pname.nspace, opcbfunc, &lock);
    PRTE_PMIX_WAIT_THREAD(&lock);
    PRTE_PMIX_DESTRUCT_LOCK(&lock);

    if (!prte_persistent) {
        /* update our exit status */
        PRTE_UPDATE_EXIT_STATUS(jdata->exit_code);
        /* if this is an abnormal termination, report it */
        if (jdata->state > PRTE_JOB_STATE_ERROR) {
            char *msg;
            msg = prte_dump_aborted_procs(jdata);
            if (NULL != msg) {
                pmix_byte_object_t bo;
                PMIX_BYTE_OBJECT_CONSTRUCT(&bo);
                bo.bytes = (char *) msg;
                bo.size = strlen(msg);
                PRTE_PMIX_CONSTRUCT_LOCK(&lock);
                rc = PMIx_server_IOF_deliver(&prte_process_info.myproc,
                                             PMIX_FWD_STDDIAG_CHANNEL,
                                             &bo, NULL, 0, lkcbfunc, (void *) &lock);
                if (PMIX_SUCCESS != rc) {
                    PMIX_ERROR_LOG(rc);
                } else {
                    /* wait for completion */
                    PRTE_PMIX_WAIT_THREAD(&lock);
                    if (PMIX_SUCCESS != lock.status) {
                        PMIX_ERROR_LOG(lock.status);
                    }
                }
                PRTE_PMIX_DESTRUCT_LOCK(&lock);
                free(msg);
            }
        }
        /* if all of the jobs we are running are done, then shut us down */
        for (i = 0; i < prte_job_data->size; i++) {
            jptr = (prte_job_t *) pmix_pointer_array_get_item(prte_job_data, i);
            if (NULL == jptr) {
                continue;
            }
            /* skip the daemon job */
            if (PMIX_CHECK_NSPACE(jptr->nspace, PRTE_PROC_MY_NAME->nspace)) {
                continue;
            }
            if (jptr->state < PRTE_JOB_STATE_TERMINATED) {
                /* still alive - finish processing this job's termination */
                goto release;
            }
        }

        /* Let the tools know that a job terminated before we shutdown */
        if (jdata->state != PRTE_JOB_STATE_NOTIFIED) {
            PMIX_OUTPUT_VERBOSE((2, prte_state_base_framework.framework_output,
                                 "%s state:dvm:check_job_completed state is terminated - activating notify",
                                 PRTE_NAME_PRINT(PRTE_PROC_MY_NAME)));
            terminate_dvm = true;  // flag that the DVM is to terminate
            PRTE_ACTIVATE_JOB_STATE(jdata, PRTE_JOB_STATE_NOTIFY_COMPLETED);
            PMIX_RELEASE(caddy);
            return;
        }

        /* if we fell thru to this point, then nobody is still
         * alive except the daemons, so just shut us down */
        prte_plm.terminate_orteds();
        PMIX_RELEASE(caddy);
        return;
    }

    if (NULL != prte_data_server_uri) {
        /* tell the data server to purge any data from this nspace */
        PMIX_DATA_BUFFER_CREATE(buf);
        /* room number is ignored, but has to be included for pack sequencing */
        i = 0;
        rc = PMIx_Data_pack(NULL, buf, &i, 1, PMIX_INT);
        if (PMIX_SUCCESS != rc) {
            PMIX_ERROR_LOG(rc);
            PMIX_DATA_BUFFER_RELEASE(buf);
            goto release;
        }
        rc = PMIx_Data_pack(NULL, buf, &command, 1, PMIX_UINT8);
        if (PMIX_SUCCESS != rc) {
            PMIX_ERROR_LOG(rc);
            PMIX_DATA_BUFFER_RELEASE(buf);
            goto release;
        }
        /* pack the nspace to be purged */
        pname.rank = PMIX_RANK_WILDCARD;
        rc = PMIx_Data_pack(NULL, buf, &pname, 1, PMIX_PROC);
        if (PMIX_SUCCESS != rc) {
            PMIX_ERROR_LOG(rc);
            PMIX_DATA_BUFFER_RELEASE(buf);
            goto release;
        }
        /* send it to the data server */
        PRTE_RML_SEND(rc, PRTE_PROC_MY_NAME->rank, buf, PRTE_RML_TAG_DATA_SERVER);
        if (PRTE_SUCCESS != rc) {
            PRTE_ERROR_LOG(rc);
            PMIX_DATA_BUFFER_RELEASE(buf);
        }
    }

release:
    /* Release the resources used by this job. Since some errmgrs may want
     * to continue using resources allocated to the job as part of their
     * fault recovery procedure, we only do this once the job is "complete".
     * Note that an aborted/killed job -is- flagged as complete and will
     * therefore have its resources released. We need to do this after
     * we call the errmgr so that any attempt to restart the job will
     * avoid doing so in the exact same place as the current job
     */
    session = jdata->session;
    if(NULL != session){
        for(i = 0; i < session->jobs->size; i++){
            if(NULL != (jptr = pmix_pointer_array_get_item(session->jobs, i))){
                if(PMIX_CHECK_NSPACE(jdata->nspace, jptr->nspace)){
                    pmix_pointer_array_set_item(session->jobs, i, NULL);
                    break;
                }
            }
        }
    }
    if (NULL != jdata->map) {
        map = jdata->map;
        takeall = false;
        if (prte_get_attribute(&jdata->attributes, PRTE_JOB_HWT_CPUS, NULL, PMIX_BOOL)) {
            type = HWLOC_OBJ_PU;
        } else {
            type = HWLOC_OBJ_CORE;
        }
        if (prte_get_attribute(&jdata->attributes, PRTE_JOB_PES_PER_PROC, NULL, PMIX_UINT16) ||
            PRTE_MAPPING_BYUSER == PRTE_GET_MAPPING_POLICY(map->mapping) ||
            PRTE_MAPPING_SEQ == PRTE_GET_MAPPING_POLICY(map->mapping)) {
            takeall = true;
        }
        boundcpus = hwloc_bitmap_alloc();
        for (index = 0; index < map->nodes->size; index++) {
            node = (prte_node_t *) pmix_pointer_array_get_item(map->nodes, index);
            if (NULL == node) {
                continue;
            }
            PMIX_OUTPUT_VERBOSE((2, prte_state_base_framework.framework_output,
                                 "%s state:dvm releasing procs from node %s",
                                 PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), node->name));
            for (i = 0; i < node->procs->size; i++) {
                proc = (prte_proc_t *) pmix_pointer_array_get_item(node->procs, i);
                if (NULL == proc) {
                    continue;
                }
                if (!PMIX_CHECK_NSPACE(proc->name.nspace, jdata->nspace)) {
                    /* skip procs from another job */
                    continue;
                }
                app = (prte_app_context_t*) pmix_pointer_array_get_item(jdata->apps, proc->app_idx);
                if (!PRTE_FLAG_TEST(app, PRTE_APP_FLAG_TOOL) &&
                    !PRTE_FLAG_TEST(jdata, PRTE_JOB_FLAG_TOOL)) {
                    node->num_procs--;
                    node->slots_inuse--;
                    node->next_node_rank--;
                }
                /* release the resources held by the proc - only the first
                 * cpu in the proc's cpuset was used to mark usage */
                if (NULL != proc->cpuset) {
                    if (0 != (rc = hwloc_bitmap_list_sscanf(boundcpus, proc->cpuset))) {
                        pmix_output(0, "hwloc_bitmap_sscanf returned %s for the string %s",
                                    prte_strerror(rc), proc->cpuset);
                        continue;
                    }
                    if (takeall) {
                        tgt = boundcpus;
                    } else {
                        /* we only want to restore the first CPU of whatever region
                         * the proc was bound to, so we have to first narrow the
                         * bitmap down to only that region */
                        hwloc_bitmap_andnot(prte_rmaps_base.available, boundcpus, node->available);
                        /* the set bits in the result are the bound cpus that are still
                         * marked as in-use */
                        obj = hwloc_get_obj_inside_cpuset_by_type(node->topology->topo,
                                                                  prte_rmaps_base.available, type, 0);
                        if (NULL == obj) {
                            pmix_output(0, "COULD NOT GET BOUND CPU FOR RESOURCE RELEASE");
                            continue;
                        }
#if HWLOC_API_VERSION < 0x20000
                        tgt = obj->allowed_cpuset;
#else
                        tgt = obj->cpuset;
#endif
                    }
                    hwloc_bitmap_or(node->available, node->available, tgt);
                }

                PMIX_OUTPUT_VERBOSE((2, prte_state_base_framework.framework_output,
                                     "%s state:dvm releasing proc %s from node %s",
                                     PRTE_NAME_PRINT(PRTE_PROC_MY_NAME),
                                     PRTE_NAME_PRINT(&proc->name), node->name));
                /* set the entry in the node array to NULL */
                pmix_pointer_array_set_item(node->procs, i, NULL);
                /* release the proc once for the map entry */
                PMIX_RELEASE(proc);
            }
            /* set the node location to NULL */
            pmix_pointer_array_set_item(map->nodes, index, NULL);
            /* maintain accounting */
            PMIX_RELEASE(node);
            /* flag that the node is no longer in a map */
            PRTE_FLAG_UNSET(node, PRTE_NODE_FLAG_MAPPED);
        }
        hwloc_bitmap_free(boundcpus);
        PMIX_RELEASE(map);
        jdata->map = NULL;
    }

    /* if requested, check fd status for leaks */
    if (prte_state_base.run_fdcheck) {
        prte_state_base_check_fds(jdata);
    }

    /* if this job was a launcher, then we need to abort all of its
     * child jobs that might still be running */
    if (0 < pmix_list_get_size(&jdata->children)) {
        PMIX_CONSTRUCT(&procs, pmix_pointer_array_t);
        pmix_pointer_array_init(&procs, 1, INT_MAX, 1);
        PMIX_LIST_FOREACH(jptr, &jdata->children, prte_job_t)
        {
            proc = PMIX_NEW(prte_proc_t);
            PMIX_LOAD_PROCID(&proc->name, jptr->nspace, PMIX_RANK_WILDCARD);
            pmix_pointer_array_add(&procs, proc);
        }
        prte_plm.terminate_procs(&procs);
        for (i = 0; i < procs.size; i++) {
            if (NULL != (proc = (prte_proc_t *) pmix_pointer_array_get_item(&procs, i))) {
                PMIX_RELEASE(proc);
            }
        }
        PMIX_DESTRUCT(&procs);
    }

    if (jdata->state != PRTE_JOB_STATE_NOTIFIED) {
        PMIX_OUTPUT_VERBOSE((2, prte_state_base_framework.framework_output,
                             "%s state:dvm:check_job_completed state is terminated - activating notify",
                             PRTE_NAME_PRINT(PRTE_PROC_MY_NAME)));
        PRTE_ACTIVATE_JOB_STATE(jdata, PRTE_JOB_STATE_NOTIFY_COMPLETED);
        /* mark the job as notified */
        jdata->state = PRTE_JOB_STATE_NOTIFIED;
    }

    PMIX_POST_OBJECT(jdata);
    PMIX_RELEASE(caddy);
}

static void cleanup_job(int sd, short args, void *cbdata)
{
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    PRTE_HIDE_UNUSED_PARAMS(sd, args);

    PMIX_ACQUIRE_OBJECT(caddy);

    if (terminate_dvm && !dvm_terminated) {
        dvm_terminated = true;
        prte_plm.terminate_orteds();
    }
    if (NULL != caddy->jdata) {
        PMIX_RELEASE(caddy->jdata);
    }
    PMIX_RELEASE(caddy);
}

static void dvm_notify(int sd, short args, void *cbdata)
{
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_job_t *jdata = caddy->jdata;
    prte_proc_t *pptr = NULL;
    int rc;
    pmix_data_buffer_t *reply;
    prte_daemon_cmd_flag_t command;
    prte_grpcomm_signature_t sig;
    bool notify = true, flag;
    pmix_proc_t *proc, pnotify;
    pmix_info_t *info;
    size_t ninfo;
    pmix_proc_t pname;
    pmix_data_buffer_t pbkt;
    pmix_data_range_t range = PMIX_RANGE_SESSION;
    pmix_status_t code, ret;
    char *errmsg = NULL;
    PRTE_HIDE_UNUSED_PARAMS(sd, args);

    PMIX_OUTPUT_VERBOSE((2, prte_state_base_framework.framework_output,
                         "%s state:dvm:dvm_notify called",
                         PRTE_NAME_PRINT(PRTE_PROC_MY_NAME)));

    /* see if there was any problem */
    if (prte_get_attribute(&jdata->attributes, PRTE_JOB_ABORTED_PROC, (void **) &pptr, PMIX_POINTER)
        && NULL != pptr) {
        rc = jdata->exit_code;
        /* or whether we got cancelled by the user */
    } else if (prte_get_attribute(&jdata->attributes, PRTE_JOB_CANCELLED, NULL, PMIX_BOOL)) {
        rc = PRTE_ERR_JOB_CANCELLED;
    } else {
        rc = jdata->exit_code;
    }

    if (0 == rc &&
        prte_get_attribute(&jdata->attributes, PRTE_JOB_SILENT_TERMINATION, NULL, PMIX_BOOL)) {
        notify = false;
    }
    /* if the jobid matches that of the requestor, then don't notify */
    if (prte_get_attribute(&jdata->attributes, PRTE_JOB_LAUNCH_PROXY, (void **) &proc, PMIX_PROC)) {
        if (PMIX_CHECK_NSPACE(proc->nspace, jdata->nspace)) {
            notify = false;
        }
        PMIX_PROC_RELEASE(proc);
    }


    if (notify) {
        /* if it was an abnormal termination, then construct an appropriate
         * error message */
        if (PRTE_SUCCESS != rc) {
            errmsg = prte_dump_aborted_procs(jdata);
        }
        /* construct the info to be provided */
        if (NULL == errmsg) {
            ninfo = 3;
        } else {
            ninfo = 4;
        }
        PMIX_INFO_CREATE(info, ninfo);
        /* ensure this only goes to the job terminated event handler */
        flag = true;
        PMIX_INFO_LOAD(&info[0], PMIX_EVENT_NON_DEFAULT, &flag, PMIX_BOOL);
        /* provide the status */
        PMIX_INFO_LOAD(&info[1], PMIX_JOB_TERM_STATUS, &rc, PMIX_STATUS);
        /* tell the requestor which job or proc  */
        PMIX_LOAD_NSPACE(pname.nspace, jdata->nspace);
        if (NULL != pptr) {
            pname.rank = pptr->name.rank;
        } else {
            pname.rank = PMIX_RANK_WILDCARD;
        }
        PMIX_INFO_LOAD(&info[2], PMIX_EVENT_AFFECTED_PROC, &pname, PMIX_PROC);
        if (NULL != errmsg) {
            PMIX_INFO_LOAD(&info[3], PMIX_EVENT_TEXT_MESSAGE, errmsg, PMIX_STRING);
            free(errmsg);
        }

        /* pack the info for sending */
        PMIX_DATA_BUFFER_CONSTRUCT(&pbkt);

        /* pack the status code */
        code = PMIX_EVENT_JOB_END;
        if (PMIX_SUCCESS != (ret = PMIx_Data_pack(NULL, &pbkt, &code, 1, PMIX_STATUS))) {
            PMIX_ERROR_LOG(ret);
            PMIX_INFO_FREE(info, ninfo);
            PMIX_DATA_BUFFER_DESTRUCT(&pbkt);
            PMIX_RELEASE(caddy);
            return;
        }
        /* pack the source - it cannot be me as that will cause
         * the pmix server to upcall the event back to me */
        PMIX_LOAD_PROCID(&pnotify, jdata->nspace, 0);
        if (PMIX_SUCCESS != (ret = PMIx_Data_pack(NULL, &pbkt, &pnotify, 1, PMIX_PROC))) {
            PMIX_ERROR_LOG(ret);
            PMIX_INFO_FREE(info, ninfo);
            PMIX_DATA_BUFFER_DESTRUCT(&pbkt);
            PMIX_RELEASE(caddy);
            return;
        }
        /* pack the range */
        if (PMIX_SUCCESS != (ret = PMIx_Data_pack(NULL, &pbkt, &range, 1, PMIX_DATA_RANGE))) {
            PMIX_ERROR_LOG(ret);
            PMIX_INFO_FREE(info, ninfo);
            PMIX_DATA_BUFFER_DESTRUCT(&pbkt);
            PMIX_RELEASE(caddy);
            return;
        }
        /* pack the number of infos */
        if (PMIX_SUCCESS != (ret = PMIx_Data_pack(NULL, &pbkt, &ninfo, 1, PMIX_SIZE))) {
            PMIX_ERROR_LOG(ret);
            PMIX_INFO_FREE(info, ninfo);
            PMIX_DATA_BUFFER_DESTRUCT(&pbkt);
            PMIX_RELEASE(caddy);
            return;
        }
        /* pack the infos themselves */
        if (PMIX_SUCCESS != (ret = PMIx_Data_pack(NULL, &pbkt, info, ninfo, PMIX_INFO))) {
            PMIX_ERROR_LOG(ret);
            PMIX_INFO_FREE(info, ninfo);
            PMIX_DATA_BUFFER_DESTRUCT(&pbkt);
            PMIX_RELEASE(caddy);
            return;
        }
        PMIX_INFO_FREE(info, ninfo);

        /* insert into pmix_data_buffer_t */
        PMIX_DATA_BUFFER_CREATE(reply);
        /* we need to add a flag indicating this came from an invalid proc so that we will
         * inject it into our own PMIx server library */
        rc = PMIx_Data_pack(NULL, reply, &PRTE_NAME_INVALID->rank, 1, PMIX_PROC_RANK);
        if (PMIX_SUCCESS != rc) {
            PMIX_ERROR_LOG(rc);
            PMIX_DATA_BUFFER_DESTRUCT(&pbkt);
            PMIX_DATA_BUFFER_RELEASE(reply);
            PMIX_RELEASE(caddy);
            return;
        }
        rc = PMIx_Data_copy_payload(reply, &pbkt);
        PMIX_DATA_BUFFER_DESTRUCT(&pbkt);

        if (PMIX_SUCCESS != rc) {
            PMIX_ERROR_LOG(rc);
            PMIX_DATA_BUFFER_RELEASE(reply);
            PMIX_RELEASE(caddy);
            return;
        }

        /* we have to send the notification to all daemons so that
         * anyone watching for it can receive it */
        PMIX_CONSTRUCT(&sig, prte_grpcomm_signature_t);
        PMIX_PROC_CREATE(sig.signature, 1);
        PMIX_LOAD_PROCID(&sig.signature[0], PRTE_PROC_MY_NAME->nspace, PMIX_RANK_WILDCARD);
        sig.sz = 1;
        if (PRTE_SUCCESS != (rc = prte_grpcomm.xcast(&sig, PRTE_RML_TAG_NOTIFICATION, reply))) {
            PRTE_ERROR_LOG(rc);
            PMIX_DATA_BUFFER_RELEASE(reply);
            PMIX_PROC_FREE(sig.signature, 1);
            PMIX_RELEASE(caddy);
            return;
        }

        PMIX_DATA_BUFFER_RELEASE(reply);
        /* maintain accounting */
        PMIX_PROC_FREE(sig.signature, 1);
    }

    if (prte_persistent) {
        /* now ensure that _all_ daemons know that this job has terminated so even
         * those that did not participate in it will know to cleanup the resources
         * they assigned to the job. This is necessary now that the mapping function
         * has been moved to the backend daemons - otherwise, non-participating daemons
         * retain the slot assignments on the participating daemons, and then incorrectly
         * map subsequent jobs thinking those nodes are still "busy" */
        PMIX_DATA_BUFFER_CREATE(reply);
        command = PRTE_DAEMON_DVM_CLEANUP_JOB_CMD;
        rc = PMIx_Data_pack(NULL, reply, &command, 1, PMIX_UINT8);
        if (PMIX_SUCCESS != rc) {
            PMIX_ERROR_LOG(rc);
            PMIX_DATA_BUFFER_RELEASE(reply);
            return;
        }
        rc = PMIx_Data_pack(NULL, reply, &jdata->nspace, 1, PMIX_PROC_NSPACE);
        if (PMIX_SUCCESS != rc) {
            PMIX_ERROR_LOG(rc);
            PMIX_DATA_BUFFER_RELEASE(reply);
            return;
        }
        PMIX_PROC_CREATE(sig.signature, 1);
        PMIX_LOAD_PROCID(&sig.signature[0], PRTE_PROC_MY_NAME->nspace, PMIX_RANK_WILDCARD);
        sig.sz = 1;
        prte_grpcomm.xcast(&sig, PRTE_RML_TAG_DAEMON, reply);
        PMIX_DATA_BUFFER_RELEASE(reply);
        PMIX_PROC_FREE(sig.signature, 1);
    }

    // We are done with our use of job data and have notified the other daemons
    if (notify) {
        PRTE_ACTIVATE_JOB_STATE(jdata, PRTE_JOB_STATE_NOTIFIED);
    }

    PMIX_RELEASE(caddy);
}

static pmix_status_t send_setop_update(prte_setop_t *setop){
    pmix_status_t rc;
    pmix_data_buffer_t buf;
    prte_grpcomm_signature_t sig;
    prte_daemon_cmd_flag_t command = PRTE_DAEMON_UPDATE_SETOP_CMD;
    
    PMIX_DATA_BUFFER_CONSTRUCT(&buf);

    /* pack the command */
    rc = PMIx_Data_pack(NULL, &buf, &command, 1, PMIX_UINT8);
    if (PMIX_SUCCESS != rc) {
        PMIX_ERROR_LOG(rc);
        PMIX_DATA_BUFFER_DESTRUCT(&buf);
        return rc;
    }
    rc = prte_setop_pack(&buf, setop);
    if(rc != PMIX_SUCCESS){
        PMIX_ERROR_LOG(rc);
        PMIX_DATA_BUFFER_DESTRUCT(&buf);
        return rc;
    }
    /* xcast it to everyone */
    PMIX_CONSTRUCT(&sig, prte_grpcomm_signature_t);
    sig.signature = (pmix_proc_t *) malloc(sizeof(pmix_proc_t));
    PMIX_LOAD_PROCID(&sig.signature[0], PRTE_PROC_MY_NAME->nspace, PMIX_RANK_WILDCARD);
    sig.sz = 1;

    if (PRTE_SUCCESS != (rc = prte_grpcomm.xcast(&sig, PRTE_RML_TAG_DAEMON, &buf))) {
        PMIX_ERROR_LOG(rc);
        PMIX_DATA_BUFFER_DESTRUCT(&buf);
        return rc;
    }
    PMIX_DESTRUCT(&sig);
    PMIX_DATA_BUFFER_DESTRUCT(&buf);

    return PRTE_SUCCESS;
}

/* Add setop to global array, inform daemons, progress state machine */
static void prte_state_setop_init(int sd, short args, void *cbdata){
    pmix_status_t rc;
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_setop_t *setop = caddy->setop;
    prte_pset_t *pset;

    PRTE_HIDE_UNUSED_PARAMS(sd, args);

    setop->state = caddy->setop_state;

    if(PRTE_SUCCESS != (rc = prte_set_setop_object(setop))){
        PRTE_ERROR_LOG(rc);
        setop->error_code = rc;
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);
        PMIX_RELEASE(caddy);
        return;
    }

    if(PRTE_SUCCESS != (rc = send_setop_update(setop))){
        PRTE_ERROR_LOG(rc);
        setop->error_code = rc;
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);
        PMIX_RELEASE(caddy);
        return;
    }

    /* Progress to next state depending on necessary resource actions */
    if(setop->op == PMIX_PSETOP_SUB || setop->op == PMIX_PSETOP_SHRINK || setop->op == PMIX_PSETOP_REPLACE){
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_MARK_SUB_PROCS);
    }else if(setop->op == PMIX_PSETOP_ADD || setop->op == PMIX_PSETOP_GROW){
        if(NULL != (pset = prte_get_pset_object(setop->input_names[0])) && !PRTE_FLAG_TEST(pset, PRTE_PSET_FLAG_NULL)){
            PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ADD_HOSTS);
        }
    }else if(setop->op == PMIX_PSETOP_CANCEL){
        /* We do not support CANCEL OPERATIONS WITHOUT SCHEDULER (yet) */
        if(!prte_pmix_server_globals.scheduler_connected){
            setop->error_code = PMIX_ERR_ALLOC_CANCELED;
            PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);
        }else{
            PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_FINALIZED);
        }
    }else{
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_FINALIZED);
    } 

    PMIX_RELEASE(caddy);   
}

/* Mark procs with PRTE_PROC_SUB_SETOP for tracking their termination */
static void prte_state_setop_mark_sub_procs(int sd, short args, void *cbdata){
    int n;
    size_t i;
    prte_proc_t *proc;
    prte_job_t *jdata;
    prte_pset_t *pset;
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_setop_t *setop = caddy->setop;
    
    PRTE_HIDE_UNUSED_PARAMS(sd, args);

    setop->state = caddy->setop_state;

    if( PMIX_PSETOP_SUB == setop->op && 
        0 == strcmp("", setop->output_names[0])){
        
        pset = prte_get_pset_object(setop->input_names[0]);
    }else if (  PMIX_PSETOP_SUB == setop->op || 
                PMIX_PSETOP_SHRINK == setop->op || 
                PMIX_PSETOP_REPLACE == setop->op){
        
        pset = prte_get_pset_object(setop->output_names[0]);
    }

    /* Mark procs to terminate */
    if(NULL != pset && 0 < pset->num_members){
        jdata = prte_get_job_data_object(setop->nspace);
        for(n = 0; n < jdata->procs->size; n++){
            if(NULL == (proc = pmix_pointer_array_get_item(jdata->procs, n))){
                continue;
            }
            for(i = 0; i < pset->num_members; i++){
                if(PMIX_CHECK_PROCID(&proc->name, &pset->members[i])){
                    prte_set_attribute(&proc->attributes, PRTE_PROC_SUB_SETOP, PRTE_ATTR_LOCAL, (void *) setop->alloc_id, PMIX_STRING);
                    ++setop->n_procs_term;
                }
            }
        }
    }

    if(setop->op == PMIX_PSETOP_REPLACE && 0 != strcmp(setop->output_names[1], "")){
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ADD_HOSTS);
    }else{
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_PENDING);
    }
    PMIX_RELEASE(caddy);    
}

/* Add host to the nodepool as defined by the setop */
static void prte_state_setop_add_hosts(int sd, short args, void *cbdata){
    int n, i;
    pmix_status_t rc;
    prte_job_t *jdata;
    prte_node_t *node;
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_setop_t *setop = caddy->setop;
    prte_app_context_t *app;

    int num_spec_nodes = 0;
    char **nodes = NULL, **ppr = NULL, **nodes_add = NULL;
    char *tmp, *tmp2;
    bool found;

    PRTE_HIDE_UNUSED_PARAMS(sd, args);

    /* Update state */
    setop->state = caddy->setop_state;

    jdata = prte_get_job_data_object(setop->nspace);
    app = jdata->apps->addr[0]; // TODO: multi app context

    /* Get the list of nodes */
    for(n = 0; n < (int) setop->n_col; n++){
        if(PMIX_CHECK_KEY(&setop->col[n], PMIX_NODE_LIST)){
            nodes = pmix_argv_split(setop->col[n].value.data.string, ',');
            num_spec_nodes = pmix_argv_count(nodes);
        }else if(PMIX_CHECK_KEY(&setop->col[n], PMIX_PPR)){
            ppr = pmix_argv_split(setop->col[n].value.data.string, ',');
        }
    }

    /* Find nodes that will be added to the node pool */
    for(n = 0; n < num_spec_nodes; n++){
        found = false;
        for(i = 0; i < prte_node_pool->size; i++){
            if(NULL == (node = (prte_node_t *) pmix_pointer_array_get_item(prte_node_pool, i))){
                continue;
            }
            if( 0 == strcmp(nodes[n], node->name)){
                /* If they want to add the hnp. Just set the slot count */
                if(!prte_hnp_is_allocated && i == 0){
                PMIX_OUTPUT_VERBOSE(
                    (2, prte_state_base_framework.framework_output,
                     "%s setop_state_add_hosts: Adding HNP to allocation",
                     PRTE_NAME_PRINT(PRTE_PROC_MY_NAME)));
                    prte_hnp_is_allocated = true;
                    node->slots = atoi(ppr[n]);
                }
                found = true;
                break;
            }
        }
        if(!found){
            tmp = malloc(strlen(nodes[n]) + strlen(":") + strlen(ppr[n]) + 1);
            strcpy(tmp, nodes[n]);
            strcat(tmp, ":");
            strcat(tmp, ppr[n]);
            PMIX_ARGV_APPEND_UNIQUE_COMPAT(&nodes_add, tmp);
            free(tmp);
        }
    }

    /* If there are no new nodes to add go ahead and add the new processes */
    if(0 == PMIX_ARGV_COUNT_COMPAT(nodes_add)){
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ADD_PROCS);
        
        PMIX_ARGV_FREE_COMPAT(nodes);
        PMIX_ARGV_FREE_COMPAT(ppr);
        PMIX_RELEASE(caddy);
        return;
    }

    /* Add any new node to the node pool */
    if(0 < PMIX_ARGV_COUNT_COMPAT(nodes_add)){
        /* Update add_host and dash_host list */
        tmp = PMIX_ARGV_JOIN_COMPAT(nodes_add, ',');
        prte_set_attribute(&app->attributes, PRTE_APP_ADD_HOST, PRTE_ATTR_LOCAL,
                               (void *) tmp, PMIX_STRING);
        if(prte_get_attribute(&app->attributes, PRTE_APP_DASH_HOST, (void **) &tmp2, PMIX_STRING)){
            tmp2 = realloc(tmp2, strlen(tmp2) + strlen(",") + strlen(tmp) + 1);
            strcat(tmp2, ",");
            strcat(tmp2, tmp);
        }else{
            strcpy(tmp2, tmp);
        }
        prte_set_attribute(&app->attributes, PRTE_APP_DASH_HOST, PRTE_ATTR_LOCAL,
                       (void *) tmp2, PMIX_STRING);
        free(tmp);
        free(tmp2);

        rc = prte_ras_base_add_hosts(jdata);
        if(PRTE_SUCCESS != rc){
            PRTE_ERROR_LOG(rc);
            goto ERROR;
        }
    }

    /* Launch deamons on the new nodes*/
    PRTE_ACTIVATE_JOB_STATE(jdata, PRTE_JOB_STATE_LAUNCH_DAEMONS);
    
    PMIX_ARGV_FREE_COMPAT(nodes_add);
    PMIX_ARGV_FREE_COMPAT(nodes);
    PMIX_ARGV_FREE_COMPAT(ppr);
    PMIX_RELEASE(caddy);
    return;

ERROR:
    setop->error_code = rc;
    PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);

    PMIX_ARGV_FREE_COMPAT(nodes_add);
    PMIX_ARGV_FREE_COMPAT(nodes);
    PMIX_ARGV_FREE_COMPAT(ppr);
    PMIX_RELEASE(caddy);
    return;    
}

/* Add processes to the job as defined by the setop and activate PRTE_JOB_STATE_LAUNCH_APPS */
static void prte_state_setop_add_procs(int sd, short args, void *cbdata){
    int n, i, j, k, slots_req;
    pmix_status_t rc;
    prte_job_t *jdata;
    prte_node_t *node;
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_setop_t *setop = caddy->setop;
    prte_proc_t *proc;
    prte_app_context_t *app;

    prte_pset_t *delta_pset = NULL;
    int num_spec_nodes = 0;
    char **nodes = NULL, **ppr = NULL;


    PRTE_HIDE_UNUSED_PARAMS(sd, args);

    /* Update state */
    setop->state = caddy->setop_state;

    jdata = prte_get_job_data_object(setop->nspace);
    app = jdata->apps->addr[0]; // TODO: multi app context
    
    if(PMIX_PSETOP_ADD == setop->op || PMIX_PSETOP_GROW == setop->op){
        delta_pset = prte_get_pset_object(setop->output_names[0]);
    }else if(PMIX_PSETOP_REPLACE == setop->op){
        delta_pset = prte_get_pset_object(setop->output_names[1]);
    }else{
        rc = PMIX_ERR_BAD_PARAM;
        PRTE_ERROR_LOG(PMIX_ERR_BAD_PARAM);
        goto ERROR;
    }

    /* Get the list of nodes */
    for(n = 0; n < (int) setop->n_col; n++){
        if(PMIX_CHECK_KEY(&setop->col[n], PMIX_NODE_LIST)){
            nodes = pmix_argv_split(setop->col[n].value.data.string, ',');
            num_spec_nodes = pmix_argv_count(nodes);
        }else if(PMIX_CHECK_KEY(&setop->col[n], PMIX_PPR)){
            ppr = pmix_argv_split(setop->col[n].value.data.string, ',');
        }
    }


    /* Mark all nodes as mapped so we know if it needs to be added */
    for(n = 0; n < jdata->map->nodes->size; n++){
        if(NULL == (node = pmix_pointer_array_get_item(jdata->map->nodes, n))){
            continue;
        }
        PRTE_FLAG_SET(node, PRTE_NODE_FLAG_MAPPED);
    }

    k = 0;
    for(n = 0; n < num_spec_nodes; n++){
        /* Find the node in the node_pool */
        /* Find a node from the DVM that is not yet assigned to the job */
        for(i = 0; i < prte_node_pool->size; i++){
            if(NULL == (node = pmix_pointer_array_get_item(prte_node_pool, i))){
                continue;
            }
            if(0 != strcmp(node->name, nodes[n])){
                continue;
            }
            slots_req = atoi(ppr[n]);
            if(slots_req > node->slots - node->slots_inuse){
            PMIX_OUTPUT_VERBOSE(
            (2, prte_state_base_framework.framework_output,
             "%s state:dvm:add_procs node=%s, req=%d, node_slots=%d, inuse=%d",
             PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), node->name, slots_req, node->slots, node->slots_inuse));                
                PMIX_ERROR_LOG(PMIX_ERR_OUT_OF_RESOURCE);
                goto ERROR;
            }

            for(j = 0; j < slots_req; j++){

                /* Create proc */
                proc = PMIX_NEW(prte_proc_t);
                PMIX_LOAD_NSPACE(proc->name.nspace, delta_pset->members[k].nspace);
                proc->name.rank = delta_pset->members[k].rank;
                proc->app_rank = delta_pset->members[k].rank;
                proc->app_idx = 0; // TODO!
                proc->state = PRTE_PROC_STATE_INIT;
                proc->parent = node->daemon->name.rank;
                prte_set_attribute(&proc->attributes, PRTE_PROC_ADD_SETOP, PRTE_ATTR_LOCAL, setop->alloc_id, PMIX_STRING);
                ++setop->n_procs_launch;
                /* set node info */
                PMIX_RETAIN(node);
                proc->node = node;
                proc->node_rank = node->slots_inuse;
                proc->local_rank = node->slots_inuse;

                /* add proc to node */
                PMIX_RETAIN(proc);
                pmix_pointer_array_add(node->procs, proc);
                node->num_procs++;
                node->slots_inuse++;
                node->slots_available--;

                /* add proc to job */
                PMIX_RETAIN(proc);
                pmix_pointer_array_add(jdata->procs, proc);
                jdata->num_procs++;
                app->num_procs++;

                /* add node to job if not yet mapped*/
                if(!PRTE_FLAG_TEST(node, PRTE_NODE_FLAG_MAPPED)){
                    PMIX_RETAIN(node);
                    pmix_pointer_array_add(jdata->map->nodes, node);
                    jdata->total_slots_alloc += node->slots_available;
                    jdata->map->num_nodes++;
                    jdata->num_daemons_reported++;
                    PRTE_FLAG_SET(node, PRTE_NODE_FLAG_MAPPED);
                }

                k++;
            }

        }
    }

    /* TODO: cleanly exit setop handler. NOTE: This should not happen as we check and reserve the resources before */
    if(k != (int) delta_pset->num_members){
        rc = PMIX_ERR_OUT_OF_RESOURCE;
        PMIX_ERROR_LOG(PMIX_ERR_OUT_OF_RESOURCE);
        goto ERROR;
    }

    /* Reset mapped flag */
    for(n = 0; n < jdata->map->nodes->size; n++){
        if(NULL == (node = pmix_pointer_array_get_item(jdata->map->nodes, n))){
            continue;
        }
        PRTE_FLAG_UNSET(node, PRTE_NODE_FLAG_MAPPED);
    }

    if(PRTE_SUCCESS != (rc = send_setop_update(setop))){
        PRTE_ERROR_LOG(rc);
        goto ERROR;
    }


    /* Launch the new processes */
    PRTE_ACTIVATE_JOB_STATE(jdata, PRTE_JOB_STATE_LAUNCH_APPS);

    PMIX_ARGV_FREE_COMPAT(nodes);
    PMIX_ARGV_FREE_COMPAT(ppr);
    PMIX_RELEASE(caddy);
    return;
ERROR:
    setop->error_code = rc;
    PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);

    PMIX_ARGV_FREE_COMPAT(nodes);
    PMIX_ARGV_FREE_COMPAT(ppr);
    PMIX_RELEASE(caddy);
    return;    
}

/* Progresses the setop server as we might need to send an alloc response 
 * Setop is in pending state until:
    - receiving a PMIX_SETOP_FINALIZE event, or
    - all specified processes have terminated 
 */
static void prte_state_setop_pending(int sd, short args, void *cbdata){
    pmix_status_t rc;
    prte_pset_t *pset;
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_setop_t *setop = caddy->setop;
    PRTE_HIDE_UNUSED_PARAMS(sd, args);
    
    setop->state = caddy->setop_state;

    if(PRTE_SUCCESS != (rc = send_setop_update(setop))){
        PRTE_ERROR_LOG(rc);
        setop->error_code = rc;
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);
        PMIX_RELEASE(caddy);
        return;
    }
    
    /* A setop is pending: 
     * Progress the setop server in case we can answer the request */
    prte_setop_server_progress(setop, 1);

    /* If this was a launch PSetOp finalize it immediately */
    pset = prte_get_pset_object(setop->input_names[0]);
    if(PRTE_FLAG_TEST(pset, PRTE_PSET_FLAG_NULL)){
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_FINALIZED);
    }

    PMIX_RELEASE(caddy);
}

static pmix_status_t remove_procs(prte_setop_t * setop){
    prte_job_t * jdata;
    prte_node_t * node;
    prte_pset_t * pset;
    prte_app_context_t *app;
    prte_proc_t *job_proc;
    pmix_proc_t *proc;
    int n, i, k, count;
    

    if(PMIX_PSETOP_SUB == setop->op && 0 == strcmp("", setop->output_names[0])){
        pset = prte_get_pset_object(setop->input_names[0]);
    }else if (PMIX_PSETOP_SUB == setop->op || PMIX_PSETOP_SHRINK == setop->op || PMIX_PSETOP_REPLACE == setop->op){
        pset = prte_get_pset_object(setop->output_names[0]);
    }else{
        PRTE_ERROR_LOG(PMIX_ERR_BAD_PARAM);
        return PMIX_ERR_BAD_PARAM;
    }

    /* It is not an error if we do not find the job data object
     * It might have been already released as all local procs have terminated
     */
    jdata = prte_get_job_data_object(setop->nspace);
    if(NULL == jdata || 0 == jdata->num_procs){
        return PMIX_SUCCESS;        
    }
    for(n = 0; n < (int) pset->num_members; n++){
        proc = &pset->members[n];
        /* update top level job data */
        for(i = 0; i < jdata->procs->size; i++){
            if(NULL == (job_proc = pmix_pointer_array_get_item(jdata->procs, i))){
                continue;
            }
            if(PMIX_CHECK_PROCID(proc, &job_proc->name)){
                /* as we remove the process we need to update both, num_procs and num terminated */
                --jdata->num_procs;
                --jdata->num_terminated;
                if(PMIX_CHECK_RANK(PRTE_PROC_MY_NAME->rank, job_proc->parent)){
                    --jdata->num_local_procs;
                }
                pmix_pointer_array_set_item(jdata->procs, i, NULL);
                /* Also update the number of procs in the app contexts */
                for(k = 0; k < jdata->apps->size; k++){
                    if(NULL == (app = pmix_pointer_array_get_item(jdata->apps, k))){
                        continue;
                    }
                    if(app->idx == job_proc->app_idx){
                        app->num_procs--;
                    }
                }
                PMIX_RELEASE(job_proc);
            }
            /* remove it from our child list */
            for(k = 0; k < prte_local_children->size; k++){
                if(NULL == (job_proc = pmix_pointer_array_get_item(prte_local_children, k))){
                    continue;
                }
                if(PMIX_CHECK_PROCID(proc, &job_proc->name)){
                    pmix_pointer_array_set_item(prte_local_children, k, NULL);
                    PMIX_RELEASE(job_proc);
                }
            }
        }
        /* Update the job map */
        for(i = 0; i < jdata->map->nodes->size; i++){
            if(NULL == (node = pmix_pointer_array_get_item(jdata->map->nodes, i))){
                continue;
            }
            /* update node */
            for(k = 0; k < node->procs->size; k++){
                if(NULL == (job_proc = pmix_pointer_array_get_item(node->procs, k))){
                    continue;
                }
                if(PMIX_CHECK_PROCID(proc, &job_proc->name)){
                    pmix_pointer_array_set_item(node->procs, k, NULL);
                    PMIX_RELEASE(job_proc);
                    --node->num_procs;
                    /* stack assumption */
                    --node->next_node_rank;
                    --node->slots_inuse;
                    ++node->slots_available;
                }
            }
            /* If there are no procs of this job on the node anymore remove it  */
            count = 0;
            for(k = 0; k < node->procs->size; k++){
                if(NULL == (job_proc = pmix_pointer_array_get_item(node->procs, k))){
                    continue;
                }
                if(PMIX_CHECK_NSPACE(job_proc->name.nspace, pset->members[0].nspace)){
                    ++count;
                    break;
                }
            }
            if(0 == count){
                PMIX_RELEASE(node);
                pmix_pointer_array_set_item(jdata->map->nodes, i, NULL);                
                --jdata->map->num_nodes;
            }
        }
    }
    return PMIX_SUCCESS;
}

/* Remove the terminated processes from the job and update pmix nspace in server */
static void prte_state_setop_sub_procs(int sd, short args, void *cbdata){
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_setop_t *setop = caddy->setop;
    prte_job_t *jdata;
    pmix_status_t rc;

    PRTE_HIDE_UNUSED_PARAMS(sd, args);

    setop->state = caddy->setop_state;

    /* If all processes in the job terminated we don't need to remove processes */
    if(NULL !=  (jdata = prte_get_job_data_object(setop->nspace)) &&
                !(jdata->num_terminated == jdata->num_procs)){
        /* Remove processes from jdata */
        if(PRTE_SUCCESS != (rc = remove_procs(setop))){
            PRTE_ERROR_LOG(rc);
            setop->error_code = rc;
            PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);
            PMIX_RELEASE(caddy);
            return;
        }

        /* register the updated jdata */
        jdata = prte_get_job_data_object(setop->nspace);
        if(PRTE_SUCCESS != (rc = prte_pmix_server_register_nspace(jdata))){
            PRTE_ERROR_LOG(rc);
            setop->error_code = rc;
            PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);
            PMIX_RELEASE(caddy);
            return;
        }
    }
    /* Send the setop update */
    if(PRTE_SUCCESS != (rc = send_setop_update(setop))){
        PRTE_ERROR_LOG(rc);
        setop->error_code = rc;
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);
        PMIX_RELEASE(caddy);
        return;
    }

    PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_FINALIZED);

    PMIX_RELEASE(caddy);
}

/* We received a PMIX_SETOP_FINALIZE event but not all processes are terminated yet */
static void prte_state_setop_in_finalization(int sd, short args, void *cbdata){
    pmix_status_t rc;
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_setop_t *setop = caddy->setop;

    PRTE_HIDE_UNUSED_PARAMS(sd, args);

    if(setop->state != PRTE_SETOP_STATE_PENDING){
        return;
    }

    setop->state = caddy->setop_state;

    if(PRTE_SUCCESS != (rc = send_setop_update(setop))){
        PRTE_ERROR_LOG(rc);
        setop->error_code = rc;
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);
        PMIX_RELEASE(caddy);
        return;
    }

    PMIX_RELEASE(caddy);

}

/* Progresses the setop server */
static void prte_state_setop_finalized(int sd, short args, void *cbdata){
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_setop_t *setop = caddy->setop;
    pmix_info_t *event_info;
    pmix_status_t rc;
    bool non_default = true;
    char *tmp;

    PRTE_HIDE_UNUSED_PARAMS(sd, args);

    setop->state = caddy->setop_state;

    if(PRTE_SUCCESS != (rc = send_setop_update(setop))){
        PRTE_ERROR_LOG(rc);
        setop->error_code = rc;
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);
        PMIX_RELEASE(caddy);
        return;
    }

    /* Notify local clients about the finalization */
    PMIX_INFO_CREATE(event_info, 5);
    PMIX_INFO_LOAD(&event_info[0], PMIX_EVENT_NON_DEFAULT, &non_default, PMIX_BOOL);
    PMIX_INFO_LOAD(&event_info[1], PMIX_PSET_NAME, setop->input_names[0], PMIX_STRING);
    PMIX_INFO_LOAD(&event_info[2], PMIX_ALLOC_ID, setop->alloc_id, PMIX_STRING);
    PMIX_INFO_LOAD(&event_info[3], "prte.notify.donotloop2", NULL, PMIX_BOOL);
    tmp = pmix_argv_join(setop->output_names, ',');
    PMIX_INFO_LOAD(&event_info[4], PMIX_PSETOP_OUTPUT, tmp, PMIX_STRING);
    free(tmp);
    if(PRTE_SUCCESS != (rc = PMIx_Notify_event(PMIX_PSETOP_FINALIZED, PRTE_PROC_MY_NAME, PMIX_RANGE_LOCAL, event_info, 5, NULL, NULL))){
        PRTE_ERROR_LOG(rc);
    }
    PMIX_INFO_FREE(event_info, 5);
 
    /* Progress the setop server. This will:
     *  - start to process new requests if requests waited on this setop 
     *  - release the request and the corresponding setops if all setops are finalized/errored 
     */
    prte_setop_server_progress(setop, 1);

    PMIX_RELEASE(caddy);

}

static void prte_state_setop_report_error(int sd, short args, void *cbdata){
    pmix_status_t rc;
    prte_state_caddy_t *caddy = (prte_state_caddy_t *) cbdata;
    prte_setop_t *setop = caddy->setop;

    PRTE_HIDE_UNUSED_PARAMS(sd, args);

    setop->state = PRTE_SETOP_STATE_ERROR;

    char *setop_string = NULL;
    prte_setop_print(&setop_string, setop);
    PMIX_OUTPUT_VERBOSE((2, prte_state_base_framework.framework_output,
                     "%s [SETOP ERROR] reported for setop: %s\n",
                     PRTE_NAME_PRINT(PRTE_PROC_MY_NAME), setop_string));
    free(setop_string);

    if(PRTE_SUCCESS != (rc = send_setop_update(setop))){
        PRTE_ERROR_LOG(rc);
        setop->error_code = rc;
        PRTE_ACTIVATE_SETOP_STATE(setop, PRTE_SETOP_STATE_ERROR);
        PMIX_RELEASE(caddy);
        return;
    }

    /* A Setop was finalized: 
     * Progress the setop server so we can answer/remove the request */
    prte_setop_server_progress(setop, 1);

    PMIX_RELEASE(caddy);
}