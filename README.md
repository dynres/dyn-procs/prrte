# Time-X EuroHPC project: Dynamic Runtime Environment Extensions (based on PRRTE)

This repository is based on an [prrte](https://github.com/openpmix/prrte) fork and provides extensions to support the [Time-X Dynamic MPI Interface](https://gitlab.inria.fr/dynres/dyn-procs/ompi) 

## Installation
 
To setup the dynamic Runtime Environment Extensions with the [Time-X Dynmic MPI Interface](https://gitlab.inria.fr/dynres/dyn-procs/ompi) and the [Time-X Dynamic PMIx Extensions](https://gitlab.inria.fr/dynres/dyn-procs/openpmix) it is strongly recommended to use the descriptions and setup scripts provided in the following repo:

https://gitlab.inria.fr/dynres/dyn-procs/dyn_procs_setup 
 
For a manual setup, run the following commands:

Generate the configure scripts:
`./autogen.pl`

Run the configure script:
`./configure --prefix=/path/for/pmix/install -with-pmix=/path/to/timex/dynamic/pmix --with-hwloc=/path/to/hwloc --disable-werror`

Run make:
`make -j all install`

Also see the HACKING.md file for more information on building and installing PRRTE.

## Copyright
This repository is based on a fork of the prrte repository. For copyright information see the README_original file. 
